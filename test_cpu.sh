set -e

VIVADO_BIN=/home/fabian/tools/Xilinx/Vivado/2019.1/bin
XVHDL=${VIVADO_BIN}/xvhdl
XELAB=${VIVADO_BIN}/xelab
XSIM=${VIVADO_BIN}/xsim

${XVHDL} --nolog src/nes_alu.vhdl src/nes_cpu.vhdl test/testmem.vhdl test/cpu_logger.vhdl test/nes_cpu.vhdl

${XELAB} --nolog test_nes_cpu -s test_cpu_test --debug all
${XSIM} --nolog --wdb test_cpu.wdb -t test_cpu.tcl test_cpu_test


