
import binascii
import serial
import struct

START_CHR = b'\x02'
START_SEQ = START_CHR * 2

END_CHR = b'\x03'
END_SEQ = END_CHR * 2

ESCAPE_CHR = b'\x1b'


class StartDetector:
    """
    Detects frame start conditions
    """
    def __init__(self):
        self.prev_start = False 

    def process(self, b):
        """
        Process byte

        :param b: the byte to process
        :return: True iff start condition is detected
        """
        is_start = (b == START_CHR)

        if is_start and self.prev_start:
            return True

        self.prev_start = is_start
        return False


class Comm:
    """
    Comm handles communication at the level of sending and receiving frames

    Frames carry a CRC to check validity. Invalid frames are dropped.
    """

    def __init__(self):
        self.tty = serial.serial_for_url(
            "/dev/ttyUSB1", baudrate=115200, timeout=10)
        self.start_detector = StartDetector()

    def send_data(self, data):
        """Send data properly escaped"""
        for b in data:
            bs = bytes([b])
            if bs in [START_CHR, END_CHR, ESCAPE_CHR]:
                self.tty.write(ESCAPE_CHR)
            self.tty.write(bs)

    def comm(self, request, num_tries=3):
        """
        Send a request and wait for the response

        In case no response is received, the request is attempted 2 more times.
        """
        while True:

            num_tries -= 1
            self.send(request)

            try:
                return self.recv()
            except TimeoutError:
                if num_tries == 0:
                    raise

    def send(self, frame_data):
        """Send a frame"""

        self.tty.reset_input_buffer()
        self.tty.reset_output_buffer()

        self.tty.write(START_SEQ)

        # first send crc
        crc = binascii.crc32(frame_data) & 0xffffffff
        self.send_data(struct.pack(">I", crc))

        self.send_data(frame_data)

        self.tty.write(END_SEQ)
        self.tty.flush()

    def recv(self):
        """Wait for a valid frame"""
        while True:
            resp = self.recv_frame()

            if len(resp) < 4:
                continue

            crc = struct.unpack(">I", resp[:4])[0]
            data = resp[4:]
            actual_crc = binascii.crc32(data) & 0xffffffff

            if actual_crc == crc:
                break
            else:
                print("crc mismatch")

        return data

    def recv_frame(self):
        """Receive a frame"""
        resp = bytes()
        state = "START"

        while True:
            b = self.tty.read(1)
            if not b:
                raise TimeoutError

            # Start detector must be run irregardless of the current state,
            # hence start detection is not handled in the state machine.
            if self.start_detector.process(b):
                state = "DATA"
                continue

            # Waiting for start sequence
            if state == "START":
                continue
            # Waiting for data
            elif state == "DATA":
                if b == ESCAPE_CHR:
                    state = "DATA_ESCAPE"
                elif b == END_CHR:
                    state = "END"
                elif b == START_CHR:
                    # Unexpected start byte, frame is broken.
                    state = "START"
                else:
                    resp = resp + b
            # Waiting for escaped data
            elif state == "DATA_ESCAPE":
                resp = resp + b
                state = "DATA"
            # Waiting for second end byte
            elif state == "END":
                if b == END_CHR:
                    break
                elif b == START_CHR:
                    # Unexpected byte, frame is broken.
                    state = "START"

        return resp
