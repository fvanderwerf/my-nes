#!/usr/bin/env python3

from comm import Comm

import binascii
import cmd
import json
import re
import shlex
import struct
import textwrap


class Memory:
    """Interface to access memory and registers"""

    CMD_READ = b"\x02"
    CMD_WRITE = b"\x03"
    CMD_FLUSH = b"\x04"
    CMD_READ_REG = b"\x05"
    CMD_WRITE_REG = b"\x06"

    DDR_BASE = 0x80000000
    DDR_SIZE = 0x0800000
    MAX_CHUNK_SIZE = 2048

    def __init__(self, comm):
        self.comm = comm

    @classmethod
    def chunkify(cls, data):
        offset = 0
        size_left = len(data)

        while size_left:
            if size_left > cls.MAX_CHUNK_SIZE:
                chunk_size = cls.MAX_CHUNK_SIZE
            else:
                chunk_size = size_left

            yield data[offset:offset + chunk_size]

            offset += chunk_size
            size_left -= chunk_size

    def chunk_write(self, address, data):
        """
        Write a chunk of data

        At most MAX_CHUNK_SIZE bytes can be sent
        """
        assert len(data) <= self.MAX_CHUNK_SIZE
        frame = self.CMD_WRITE + struct.pack(">I", address) + data
        self.comm.comm(frame)

    def chunk_read(self, address, length):
        """
        Read a chunk of data

        Length can be at most MAX_CHUNK_SIZE
        """
        assert length <= self.MAX_CHUNK_SIZE
        frame = self.CMD_READ + struct.pack(">II", address, length)
        return self.comm.comm(frame)

    def write(self, address, data, flush=False):
        """
        Write data to memory. If necessary data is split into multiple chunks.
        """

        for chunk in self.chunkify(data):
            self.chunk_write(address, chunk)
            address += len(chunk)

        if flush:
            self.flush(address, len(data))

    def read(self, address, length):
        """
        Read data from memory. If necessary data is requested in multiple
        chunks.
        """
        data = bytes()
        remainder = length

        while remainder > 0:
            if remainder > self.MAX_CHUNK_SIZE:
                chunk_size = self.MAX_CHUNK_SIZE
            else:
                chunk_size = remainder

            chunk = self.chunk_read(address, chunk_size)
            data = data + chunk

            address += chunk_size
            remainder -= chunk_size

        return data

    def flush(self, address, length):
        """
        Ensure data is written to memory by flushing the cache on the softcore.
        """
        frame = self.CMD_FLUSH + struct.pack(">II", address, length)
        self.comm.comm(frame)

    def read_reg(self, address):
        """
        Read register
        """
        frame = self.CMD_READ_REG + struct.pack(">I", address)
        resp = self.comm.comm(frame)
        (value,) = struct.unpack(">I", resp)
        return value

    def write_reg(self, address, value):
        """
        Write register
        """
        frame = self.CMD_WRITE_REG + struct.pack(">II", address, value)
        self.comm.comm(frame)


class Nes:
    """Provide controls to drive the NES"""

    # NES register layout
    REG_BASE = 0x10001000
    CTRL_REG_ADDR = REG_BASE + 0x0000
    STEP_REG_ADDR = REG_BASE + 0x0080
    PC_REG_ADDR = REG_BASE + 0x0084
    IR_REG_ADDR = REG_BASE + 0x0088
    P_REG_ADDR = REG_BASE + 0x008c
    A_REG_ADDR = REG_BASE + 0x0090
    S_REG_ADDR = REG_BASE + 0x0094
    X_REG_ADDR = REG_BASE + 0x0098
    Y_REG_ADDR = REG_BASE + 0x009c

    # unit size of prgrom and chrrom in iNES format
    PRGROM_UNIT = 16 * 1024
    CHRROM_UNIT = 8 * 1024

    MAX_PRGROM_SIZE = 256 * PRGROM_UNIT
    MAX_CHRROM_SIZE = 256 * CHRROM_UNIT

    MEM_BASE = 0x10010000
    PRGROM_BASE = MEM_BASE + 0x8000
    CHRROM_BASE = PRGROM_BASE + MAX_PRGROM_SIZE

    CMD_NES_STEP = b"\x81"
    CMD_NES_ISTEP = b"\x82"

    def __init__(self, memory, comm):
        self.memory = memory
        self.comm = comm

    def _parse_header(self, header):
        if header[:4] != b"\x4e\x45\x53\x1a":
            raise RuntimeError("iNES magic not found")

        prgrom_size = header[4] * self.PRGROM_UNIT
        chrrom_size = header[5] * self.CHRROM_UNIT
        mirror = 'V' if header[6] & 0x01 else 'H'

        if header[6] & 0x02:
            raise RuntimeError("Persistent PRG RAM not supported")

        if header[6] & 0x04:
            raise RuntimeError("Trainer not supported")

        if header[6] & 0x08:
            mirror = '4'

        mapper = ((header[6] >> 4) & 0x0F) | (header[7] & 0xF0)

        if ((header[7] >> 2) & 0x03) != 2:
            # not iNES 2.0, so ignore other fields
            return {
                'prgrom_size': prgrom_size,
                'chrrom_size': chrrom_size,
                'mirror': mirror,
                'mapper': mapper
            }

        raise RuntimeError("iNES 2.0 not supported")

    def rom_info(self, filename):
        "Print details of iNES rom"

        with open(filename, 'rb') as rom_file:
            rom = rom_file.read()

        if len(rom) < 16:
            print("{} too small for rom".format(filename))
            return

        header = self._parse_header(rom[:16])

        print("ROM info:")
        print(textwrap.indent(json.dumps(header, indent=True), prefix=4 * " "))

    def load_rom(self, filename):
        "Load iNES rom into the NES."

        with open(filename, 'rb') as rom_file:
            rom = rom_file.read()

        if len(rom) < 16:
            print("{} too small for rom".format(filename))
            return

        header = self._parse_header(rom[:16])

        print("ROM info:")
        print(textwrap.indent(json.dumps(header, indent=True), prefix=4 * " "))

        data = rom[16:]
        assert len(data) == header["prgrom_size"] + header["chrrom_size"]

        prgrom = data[:header["prgrom_size"]]
        chrrom = data[header["prgrom_size"]:]

        assert len(prgrom) == header["prgrom_size"]
        assert len(chrrom) == header["chrrom_size"]

        if len(prgrom) not in [16 * 1024, 32 * 1024]:
            print("Cannot handle PRGROM size {:d}".format(len(prgrom)))

        self.memory.write(self.PRGROM_BASE, prgrom)
        print("PRG ROM: loaded {:d} bytes at 0x{:08x}".format(
                    len(prgrom), self.PRGROM_BASE))

        # if the prgrom is 16KB then it is mirrored to cover 0x8000 to 0xFFFF
        # because this is read only memory, we can simply copy the memory
        # In the future may want to configure the mapper in the NES to map
        # 0xC000-0xFFFF to 0x8000-0xBFFF
        if len(prgrom) == (16 * 1024):
            self.memory.write(self.PRGROM_BASE + len(prgrom), prgrom)
            print("PRG ROM: loaded {:d} bytes at 0x{:08x}".format(
                        len(prgrom), self.PRGROM_BASE + len(prgrom)))

        # Make sure all memory writes are pushed to memory
        self.memory.flush(self.PRGROM_BASE, 32 * 1024)

        self.memory.write(self.CHRROM_BASE, chrrom, flush=True)
        print("CHR ROM: loaded {:d} bytes at 0x{:08x}".format(
                    len(chrrom), self.CHRROM_BASE))

    def read_step(self):
        return self.memory.read_reg(self. STEP_REG_ADDR)

    def read_pc(self):
        return self.memory.read_reg(self. PC_REG_ADDR)

    def read_ir(self):
        return self.memory.read_reg(self. IR_REG_ADDR)

    def read_p(self):
        return self.memory.read_reg(self. P_REG_ADDR)

    def read_a(self):
        return self.memory.read_reg(self. A_REG_ADDR)

    def read_s(self):
        return self.memory.read_reg(self. S_REG_ADDR)

    def read_x(self):
        return self.memory.read_reg(self. X_REG_ADDR)

    def read_y(self):
        return self.memory.read_reg(self. Y_REG_ADDR)

    def print_regs(self):
        reg_base = 0x10001000
        regs = [
            ("CTRL", 0x000),
            ("STATUS", 0x004),
            ("NES_CPU_STEP", 0x080),
            ("NES_CPU_PC", 0x084),
            ("NES_CPU_IR", 0x088),
            ("NES_CPU_P", 0x08c),
            ("NES_CPU_A", 0x090),
            ("NES_CPU_S", 0x094),
            ("NES_CPU_X", 0x098),
            ("NES_CPU_Y", 0x09c),

            ("NES_CPU_MEM_ADDR", 0x100),
            ("NES_CPU_MEM_DIN", 0x104),
            ("NES_CPU_MEM_DOUT", 0x108),
            ("NES_CPU_MEM_WE", 0x10c),
            ("NES_CPU_MEM_RDY", 0x110)
        ]

        for (name, offset) in regs:
            value = self.memory.read_reg(reg_base + offset)
            print("{}: 0x{:08x}".format(name, value))

    def reset(self):
        "Soft reset the NES core"
        self.memory.write_reg(self.CTRL_REG_ADDR, 0x1)
        self.memory.write_reg(self.CTRL_REG_ADDR, 0x0)

    def _parse_status(self, status):
        "Parse cpu status as returned by step and istep"
        regs = struct.unpack(">HBBBBBI", status)

        return {
            "pc": regs[0],
            "a": regs[1],
            "x": regs[2],
            "y": regs[3],
            "p": regs[4],
            "sp": regs[5],
            "cyc": regs[6]
        }

    def step(self):
        "Run the cpu for a single cycle"
        status = self.comm.comm(self.CMD_NES_STEP)
        return self._parse_status(status)

    def istep(self):
        "Run the cpu until the beginning of the next instruction"
        status = self.comm.comm(self.CMD_NES_ISTEP)
        return self._parse_status(status)


class Cmd(cmd.Cmd):

    intro = "Welcome to the nesagain. Type help to list commands."
    prompt = "> "

    def __init__(self, memory, nes):
        self.memory = memory
        self.nes = nes
        super(Cmd, self).__init__()

    def precmd(self, cmd):
        """
        Pre-command hook to handle EOF. EOF is not implemented as a normal
        command to prevent listing in the help.
        """
        if cmd == "EOF":
            print("")   # extra new line
            return "exit"
        else:
            return cmd

    def do_exit(self, arg):
        """Exit the application"""
        return True

    def do_dump(self, arg):
        """
        dump <ADDRESS> <SIZE>

        Dump memory contents

        Example: dump 0x80000000 0x1000"
        """
        args = shlex.split(arg)
        if len(args) != 2:
            print("Invalid number of parameters")
            return

        address = int(args[0], base=0)
        size = int(args[1], base=0)

        data = self.memory.read(address, size)
        hex_data = binascii.hexlify(data)

        print(textwrap.fill(hex_data.decode('utf-8'), width=32,
                            replace_whitespace=True))

    def do_read_reg(self, arg):
        """
        read_reg <ADDRESS>

        Read register

        Example: read_reg 0x10001000"
        """
        args = shlex.split(arg)
        if len(args) != 1:
            print("Invalid number of parameters")
            return

        address = int(args[0], base=0)
        value = self.memory.read_reg(address)
        print("0x{:08x}".format(value))

    def do_write_reg(self, arg):
        """
        write_reg <ADDRESS> <VALUE>

        Write register

        Example: write_reg 0x10001004 0x12345678
        """
        args = shlex.split(arg)
        if len(args) != 2:
            print("Invalid number of parameters")
            return

        address = int(args[0], base=0)
        value = int(args[1], base=0)
        self.memory.write_reg(address, value)

    def do_rom_info(self, arg):
        """
        rom_info <ROM_FILENAME>

        Prints details about the ROM

        Example: rom_info nestest.nes
        """
        args = shlex.split(arg)
        if len(args) != 1:
            print("Invalid number of parameters")
            return

        try:
            self.nes.rom_info(args[0])
        except Exception as e:
            print("Error: {}".format(e))

    def do_load_rom(self, arg):
        """
        load_rom <ROM_FILENAME>

        Load an iNES rom into the NES. The NES is stopped before loading the
        ROM.

        Example: load_rom smb.nes
        """
        args = shlex.split(arg)
        if len(args) != 1:
            print("Invalid number of parameters")
            return

        try:
            self.nes.load_rom(args[0])
        except Exception as e:
            print("Error: {}".format(e))

    def do_regs(self, arg):
        """
        dump_regs

        Print the NES registers.
        """
        args = shlex.split(arg)
        if args:
            print("Invalid number of parameters")
            return

        self.nes.print_regs()

    def do_reset(self, arg):
        """
        reset

        Reset the NES.
        """
        args = shlex.split(arg)
        if args:
            print("Invalid number of parameters")
            return

        self.nes.reset()

    def do_step(self, arg):
        """
        step [NUM_STEPS]

        Run the NES for one or more cycles.

        Example:
        step
        step 23
        """
        count = 1
        args = shlex.split(arg)

        if len(args) == 1:
            count = int(args[0])
        elif args:
            print("Invalid number of parameters")
            return

        for i in range(count):
            status = self.nes.step()

            print("{:04X} IR:{:02X} A:{:02X} X:{:02X} Y:{:02X} P:{:02X} "
                  "SP:{:02X}".format(
                        status["pc"],
                        self.nes.read_ir(),
                        status["a"],
                        status["x"],
                        status["y"],
                        status["p"],
                        status["sp"]))

    def do_istep(self, arg):
        """
        istep

        Run the NES for one instruction.

        This runs step commands until istep becomes 0x80, which is the first
        step of a new instruction. In the register print the new instruction
        is read from memory instead of the ir register. Because the
        instruction has not yet been fetched. If however an interrupt is
        active the actual executed instruction may differ.

        Example: istep
        """

        count = 1
        args = shlex.split(arg)

        if len(args) == 1:
            count = int(args[0])
        elif args:
            print("Invalid number of parameters")
            return

        for i in range(count):
            status = self.nes.istep()

            # fixup the instruction. At the start of an instruction, the
            # instruction hasn't yet been fetched from memory. So do not read
            # instruction from ir, instead read from memory.
            (instruction,) = struct.unpack(
                    "B", self.memory.read(
                        self.memory.DDR_BASE + status["pc"], 1))

            print("{:04X} {:02X} A:{:02X} X:{:02X} Y:{:02X} P:{:02X} "
                  "SP:{:02X}".format(
                        status["pc"],
                        instruction,
                        status["a"],
                        status["x"],
                        status["y"],
                        status["p"],
                        status["sp"]))

    def do_nestest(self, arg):
        """
        nestest <NESTEST_ROM> <NESTEST_LOG>

        Run nestest rom and test the execution against the nestest reference
        log. To match the reference nestest.log, the rom must be patched to
        start at address 0xc000.

        Example: nestest nestest_c000.nes nestest.log
        """

        args = shlex.split(arg)

        if len(args) != 2:
            print("Invalid number of parameters")
            return

        cycles = 0
        try:
            self.nes.load_rom(args[0])
            self.nes.reset()

            with open(args[1], "r") as log:
                loglines = log.readlines()

            for line in loglines:
                print(line, end="")
                match = re.fullmatch(
                        "(?P<pc>^[0-9A-Fa-f]{4,4}).*"
                        "A:(?P<a>[0-9A-Fa-f]{2,2}).*"
                        "X:(?P<x>[0-9A-Fa-f]{2,2}).*"
                        "Y:(?P<y>[0-9A-Fa-f]{2,2}).*"
                        "P:(?P<p>[0-9A-Fa-f]{2,2}).*"
                        "SP:(?P<sp>[0-9A-Fa-f]{2,2}).*"
                        "CYC:(?P<cyc>[0-9]+).*",
                        line.strip())

                expected = {
                    "pc": int(match.group("pc"), base=16),
                    "a": int(match.group("a"), base=16),
                    "x": int(match.group("x"), base=16),
                    "y": int(match.group("y"), base=16),
                    "p": int(match.group("p"), base=16),
                    "sp": int(match.group("sp"), base=16),
                    "cyc": int(match.group("cyc"))
                }

                actual = self.nes.istep()

                # istep only returns the number cycles executed under the istep
                # commmand. Fixup cyc, to be the total number of cycles so far.
                cycles += actual["cyc"]
                actual["cyc"] = cycles

                assert actual.keys() == expected.keys()

                error = False
                for key in actual.keys():
                    if actual[key] != expected[key]:
                        print("CPU state mismatch {}, "
                              "expected={:04x}, actual={:04x}".format(
                                    key, expected[key], actual[key]))
                        error = True

                if error:
                    return

        except Exception as e:
            print("Error: {}".format(e))
        finally:
            print("{:d} cycles executed".format(cycles))


def main():
    comm = Comm()
    mem = Memory(comm)
    nes = Nes(mem, comm)
    cmd = Cmd(mem, nes)

    cmd.cmdloop()


if __name__ == "__main__":
    main()
