
create_project -in_memory -part xc7a100tcsg324-1
set_property SOURCE_MGMT_MODE All [current_project]

set vhdl2008_files {
    ./src/clk.vhdl
    ./src/fifo.vhdl
    ./src/gray.vhdl
    ./src/reset.vhdl
    ./src/vga.vhdl
    ./src/video_convert.vhdl
    ./src/video.vhdl
    ./src/regif.vhdl
    ./src/nes_alu.vhdl
    ./src/nes_cpu.vhdl
    ./src/ctrl.vhdl
    ./src/cpu_bus.vhdl
    ./src/apb_bus.vhdl
}

read_vhdl -vhdl2008 $vhdl2008_files

# Vivado does not support VHDL-2008 as the toplevel module in a block design
read_vhdl ./src/nes.vhdl
update_compile_order

# create block design
create_bd_design -dir bd system

# setup ports
create_bd_port -dir I -type CLK ext_clk
set_property CONFIG.FREQ_HZ 100000000 [get_bd_ports ext_clk]
create_bd_port -dir I -type RST resetn
set_property CONFIG.POLARITY ACTIVE_LOW [get_bd_ports resetn]
create_bd_intf_port -vlnv xilinx.com:interface:uart_rtl:1.0 -mode master uart
create_bd_intf_port -vlnv xilinx.com:interface:ddrx_rtl:1.0 -mode master ddr

create_bd_port -from 3 -to 0 -dir O -type data vga_r
create_bd_port -from 3 -to 0 -dir O -type data vga_g
create_bd_port -from 3 -to 0 -dir O -type data vga_b
create_bd_port -dir O -type data vga_hs
create_bd_port -dir O -type data vga_vs

# set up clocking
set clk_cell [create_bd_cell -vlnv xilinx.com:ip:clk_wiz:6.0 clks]
set_property -dict [list \
    CONFIG.RESET_TYPE ACTIVE_LOW \
    CONFIG.CLK_OUT1_PORT vga_clk \
    CONFIG.CLKOUT1_USED true \
    CONFIG.CLKOUT1_REQUESTED_OUT_FREQ 25.175 \
    CONFIG.CLKOUT2_USED true \
    CONFIG.CLKOUT2_REQUESTED_OUT_FREQ 50.0 \
    CONFIG.CLK_OUT3_PORT ddr_sys_clk \
    CONFIG.CLKOUT3_USED true \
    CONFIG.CLKOUT3_REQUESTED_OUT_FREQ 200.0 \
] $clk_cell


# hardcoding the mmcm parameters gives a better vga frequency
set_property -dict [list \
    CONFIG.OVERRIDE_MMCM {true} \
    CONFIG.MMCM_DIVCLK_DIVIDE {4} \
    CONFIG.MMCM_CLKFBOUT_MULT_F {48.0} \
    CONFIG.MMCM_CLKOUT0_DIVIDE_F {47.625} \
    CONFIG.MMCM_CLKOUT1_DIVIDE {24} \
    CONFIG.MMCM_CLKOUT2_DIVIDE {6} \
] $clk_cell

# actual vga clock
set vga_hz 25196850

connect_bd_net [get_bd_ports /resetn] [get_bd_pins /clks/resetn]
connect_bd_net [get_bd_ports /ext_clk] [get_bd_pins /clks/clk_in1]

# setup reset
set reset_cell [create_bd_cell -vlnv xilinx.com:ip:proc_sys_reset:5.0 reset]
set_property CONFIG.C_AUX_RESET_HIGH 1 $reset_cell

connect_bd_net [get_bd_ports /resetn] [get_bd_pins /reset/ext_reset_in]
connect_bd_net [get_bd_pins /clks/clk_out2] [get_bd_pins /reset/slowest_sync_clk]
connect_bd_net [get_bd_pins /clks/locked] [get_bd_pins /reset/dcm_locked]

# setup axi interconnect
create_bd_cell -vlnv xilinx.com:ip:axi_interconnect:2.1 interconnect
set interconnect_cell [get_bd_cells /interconnect]
set_property -dict [list \
    CONFIG.NUM_MI 4 \
    CONFIG.NUM_SI 3 \
] $interconnect_cell

connect_bd_net [get_bd_pins /reset/interconnect_aresetn] [get_bd_pins /interconnect/aresetn]
connect_bd_net [get_bd_pins /clks/clk_out2] [get_bd_pins /interconnect/aclk]

# setup microblaze
create_bd_cell -vlnv xilinx.com:ip:microblaze:11.0 softcore
set softcore_cell [get_bd_cells /softcore]
set_property -dict [list \
    CONFIG.C_DEBUG_ENABLED 0 \
    CONFIG.C_D_AXI 1 \
    CONFIG.C_D_LMB 0 \
    CONFIG.C_I_AXI 1 \
    CONFIG.C_I_LMB 0 \
] $softcore_cell

connect_bd_net [get_bd_pins /clks/clk_out2] [get_bd_pins /softcore/clk] [get_bd_pins /interconnect/s00_aclk] [get_bd_pins /interconnect/s01_aclk]
connect_bd_net [get_bd_pins /reset/interconnect_aresetn] [get_bd_pins /interconnect/s00_aresetn] [get_bd_pins /interconnect/s01_aresetn]
connect_bd_net [get_bd_pins /reset/mb_reset] [get_bd_pins /softcore/reset]
connect_bd_intf_net [get_bd_intf_pins /interconnect/s00_axi] [get_bd_intf_pins /softcore/m_axi_dp]
connect_bd_intf_net [get_bd_intf_pins /interconnect/s01_axi] [get_bd_intf_pins /softcore/m_axi_ip]

# setup axi bram converter
create_bd_cell -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram
set axi_bram_cell [get_bd_cells /axi_bram]
set_property CONFIG.SINGLE_PORT_BRAM 1 $axi_bram_cell
connect_bd_net [get_bd_pins /clks/clk_out2] [get_bd_pins /axi_bram/s_axi_aclk] [get_bd_pins /interconnect/m00_aclk]
connect_bd_net [get_bd_pins /reset/peripheral_aresetn] [get_bd_pins /axi_bram/s_axi_aresetn] [get_bd_pins /interconnect/m00_aresetn]
connect_bd_intf_net [get_bd_intf_pins /axi_bram/s_axi] [get_bd_intf_pins /interconnect/m00_axi]

# map axi bram into softcore
create_bd_addr_seg -range 0x10000 -offset 0x0 \
    [get_bd_addr_spaces /softcore/Data] \
    [get_bd_addr_segs /axi_bram/s_axi/mem0] bram_data_seg
create_bd_addr_seg -range 0x10000 -offset 0x0 \
    [get_bd_addr_spaces /softcore/Instruction] \
    [get_bd_addr_segs /axi_bram/s_axi/mem0] bram_instr_seg

# setup BRAM
create_bd_cell -vlnv xilinx.com:ip:blk_mem_gen:8.4 bram
set bram_cell [get_bd_cells /bram]
connect_bd_intf_net [get_bd_intf_pins /axi_bram/bram_porta] [get_bd_intf_pins /bram/bram_porta]

# setup uart
set uart_cell [create_bd_cell -vlnv xilinx.com:ip:axi_uartlite:2.0 axi_uart]
set_property CONFIG.C_BAUDRATE 115200 $uart_cell

connect_bd_net [get_bd_pins /clks/clk_out2] [get_bd_pins /interconnect/m01_aclk] [get_bd_pins /axi_uart/s_axi_aclk]
connect_bd_net [get_bd_pins /reset/peripheral_aresetn] [get_bd_pins /axi_uart/s_axi_aresetn] [get_bd_pins /interconnect/m01_aresetn]
connect_bd_intf_net [get_bd_intf_pins /interconnect/m01_axi] [get_bd_intf_pins /axi_uart/s_axi]
connect_bd_intf_net [get_bd_intf_ports uart] [get_bd_intf_pins /axi_uart/uart]

# map uart into softcore
create_bd_addr_seg -range 0x1000 -offset 0x10000000 \
    [get_bd_addr_spaces /softcore/Data] \
    [get_bd_addr_segs /axi_uart/s_axi/reg] uart_data_seg
exclude_bd_addr_seg -target_address_space [get_bd_addr_spaces /softcore/Instruction] \
    [get_bd_addr_segs /axi_uart/s_axi/reg]

# setup ddr
create_bd_cell -vlnv xilinx.com:ip:mig_7series:4.2 ddr
set ddr_cell [get_bd_cells /ddr]
set_property CONFIG.XML_INPUT_FILE "/home/fabian/nexys_a7/nes_again/mig.prj" $ddr_cell

# invert ui_clk_sync_rst to feed to the interconnect aresetn and aux_reset_in.
set ddr_aresetn_cell [create_bd_cell -vlnv xilinx.com:ip:util_vector_logic:2.0 ddr_aresetn]
set_property -dict [list \
    CONFIG.C_OPERATION  not \
    CONFIG.C_SIZE       1] $ddr_aresetn_cell
connect_bd_net [get_bd_pins /ddr/ui_clk_sync_rst] [get_bd_pins /ddr_aresetn/Op1]

connect_bd_net [get_bd_pins /ddr/ui_clk] [get_bd_pins /interconnect/m02_aclk]
connect_bd_net [get_bd_pins /ddr_aresetn/Res] \
    [get_bd_pins /ddr/aresetn] \
    [get_bd_pins /interconnect/m02_aresetn]
connect_bd_net [get_bd_pins /clks/ddr_sys_clk] [get_bd_pins /ddr/sys_clk_i]
connect_bd_net [get_bd_ports resetn] [get_bd_pins /ddr/sys_rst]
connect_bd_intf_net [get_bd_intf_pins /interconnect/m02_axi] [get_bd_intf_pins /ddr/s_axi]
connect_bd_intf_net [get_bd_intf_ports ddr] [get_bd_intf_pins /ddr/DDR2]
# connect ui_clk_sync_rst to aux_reset_in. This will keep the rest of the
# softcore subsystem in reset until the axi interface of the ddr is ready.
connect_bd_net [get_bd_pins /ddr/ui_clk_sync_rst] [get_bd_pins /reset/aux_reset_in]

create_bd_addr_seg -range 0x08000000 -offset 0x80000000 \
    [get_bd_addr_spaces /softcore/Data] \
    [get_bd_addr_segs /ddr/memmap/memaddr] ddr_data_seg
create_bd_addr_seg -range 0x08000000 -offset 0x80000000 \
    [get_bd_addr_spaces /softcore/Instruction] \
    [get_bd_addr_segs /ddr/memmap/memaddr] ddr_instr_seg

# setup AXI APB bridge for NES
set axi_apb_cell [create_bd_cell -vlnv xilinx.com:ip:axi_apb_bridge:3.0 axi_apb]
report_property $axi_apb_cell
set_property -dict [list \
    CONFIG.C_M_APB_PROTOCOL apb3 \
    CONFIG.C_APB_NUM_SLAVES 2 \
] $axi_apb_cell

connect_bd_net [get_bd_pins /clks/clk_out2] [get_bd_pins /axi_apb/s_axi_aclk] [get_bd_pins /interconnect/m03_aclk]
connect_bd_net [get_bd_pins /reset/peripheral_aresetn] [get_bd_pins /interconnect/m03_aresetn]
connect_bd_intf_net [get_bd_intf_pins /interconnect/m03_axi] [get_bd_intf_pins /axi_apb/AXI4_LITE]

# setup nes
create_bd_cell -type module -reference nes nes
set_property CONFIG.CLK_DOMAIN /clks_clk_out1 [get_bd_pins /nes/nes_clk]
set_property CONFIG.PHASE 0.0 [get_bd_pins /nes/nes_clk]
set_property CONFIG.CLK_DOMAIN /clks_clk_out1 [get_bd_pins /nes/vga_clk]
set_property CONFIG.PHASE 0.0 [get_bd_pins /nes/vga_clk]
set_property CONFIG.FREQ_HZ $vga_hz [get_bd_pins /nes/vga_clk]
connect_bd_net [get_bd_pins /reset/peripheral_reset] [get_bd_pins /nes/nes_reset]
connect_bd_net [get_bd_pins /clks/clk_out2] [get_bd_pins /nes/nes_clk]
connect_bd_net [get_bd_pins /clks/vga_clk] [get_bd_pins /nes/vga_clk]
connect_bd_net [get_bd_port /vga_r] [get_bd_pins /nes/vga_r]
connect_bd_net [get_bd_port /vga_g] [get_bd_pins /nes/vga_g]
connect_bd_net [get_bd_port /vga_b] [get_bd_pins /nes/vga_b]
connect_bd_net [get_bd_port /vga_hs] [get_bd_pins /nes/vga_hs]
connect_bd_net [get_bd_port /vga_vs] [get_bd_pins /nes/vga_vs]
connect_bd_intf_net [get_bd_intf_pin /axi_apb/apb_m] [get_bd_intf_pin /nes/ri_apb_s]
connect_bd_intf_net [get_bd_intf_pin /axi_apb/apb_m2] [get_bd_intf_pin /nes/mi_apb_s]
connect_bd_net [get_bd_pin /reset/peripheral_aresetn] [get_bd_pin /interconnect/s02_aresetn]
connect_bd_net [get_bd_pin /clks/clk_out2] [get_bd_pin /interconnect/s02_aclk]

# map nes register interface into softcore data address space
create_bd_addr_seg -range 0x1000 -offset 0x10001000 \
    [get_bd_addr_spaces /softcore/Data] \
    [get_bd_addr_segs /nes/RI_APB_S/Reg] apb_ri_data_seg
exclude_bd_addr_seg -target_address_space [get_bd_addr_spaces /softcore/Instruction] \
   [get_bd_addr_segs /nes/RI_APB_S/Reg]

# map nes memory interface into softcore data address space
create_bd_addr_seg -range 0x10000 -offset 0x10010000 \
    [get_bd_addr_spaces /softcore/Data] \
    [get_bd_addr_segs /nes/MI_APB_S/Reg] apb_mi_data_seg
exclude_bd_addr_seg -target_address_space [get_bd_addr_spaces /softcore/Instruction] \
   [get_bd_addr_segs /nes/MI_APB_S/Reg]
validate_bd_design

generate_target all [get_files ./bd/system/system.bd]

read_verilog ./bd/system/hdl/system_wrapper.v

# read timing constraints
read_xdc -unmanaged ./src/timing.xdc

# generate make dependencies
set fd [open "synth.mk" "w"]
puts $fd "synth.dcp: [get_files]"
close $fd

synth_design -top system_wrapper
opt_design

check_timing
report_timing -file synth-timing.txt -warn_on_violation -max_paths 100

write_checkpoint -force synth
