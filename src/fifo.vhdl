
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fifo is
    generic(
        width : positive := 12;
        depth_shift : positive := 4
    );
    port(
        areset : in std_logic;

        -- write interface
        wclk : in std_logic;
        wrdy : out std_logic;
        wen : in std_logic;
        wdata : in std_logic_vector((width - 1) downto 0);
        full : out std_logic;

        -- read interface
        rclk : in std_logic;
        rrdy : out std_logic;
        ren : in std_logic;
        rdata : out std_logic_vector((width - 1) downto 0);
        empty : out std_logic
    );
end fifo;

architecture behavioral of fifo is
    constant depth : integer := 2 ** depth_shift;

    subtype entry is std_logic_vector(width - 1 downto 0);

    type fifo_data is array(0 to depth - 1) of entry;

    signal rreset : std_logic_vector(1 downto 0);
    signal wreset : std_logic_vector(1 downto 0);

    signal data : fifo_data;

    -- note indices are one bit wider than is necessary to address the fifo
    -- memory. This is needed to differentiate between a full and an empty
    -- fifo.
    signal rindex : std_logic_vector(depth_shift downto 0);
    signal windex : std_logic_vector(depth_shift downto 0);

    -- write index synchronized to the read clock domain
    signal rwindex : std_logic_vector(depth_shift downto 0);
    -- read index synchronized to the write clock domain
    signal wrindex : std_logic_vector(depth_shift downto 0);

    -- index constant with only the MSB set
    -- use to flip the MSB using xor, to test whether the fifo is full
    constant index_msb : std_logic_vector(depth_shift downto 0)
            := (depth_shift => '1', others => '0');
begin

    -- synchronizes the write index to the read domain
    windex_sync : entity work.gray_sync
        generic map(
            width => depth_shift + 1)
        port map(
            areset => wreset(1),
            iclk => wclk,
            i => windex,
            inc => wen,
            oclk => rclk,
            o => rwindex);

    -- synchronizes the read index to the write domain
    rindex_sync : entity work.gray_sync
        generic map(
            width => depth_shift + 1)
        port map(
            areset => rreset(1),
            iclk => rclk,
            i => rindex,
            inc => ren,
            oclk => wclk,
            o => wrindex);


    -- areset cannot be deasserted synchronously to both clock domains
    -- double flipflop the reset into the read and write clock domains.
    -- The synchronized resets are used assert the ready signal.
    process(areset, rclk, wclk)
    begin
        if areset = '1' then
            rreset <= "11";
            wreset <= "11";
        else
            if rising_edge(rclk) then
                rreset <= rreset(0) & "0";
            end if;

            if rising_edge(wclk) then
                wreset <= wreset(0) & "0";
            end if;
        end if;
    end process;

    -- the ready signals
    rrdy <= not rreset(1);
    wrdy <= not wreset(1);

    -- read process
    rdata <= data(to_integer(unsigned(rindex(depth_shift - 1 downto 0))));
    process(rindex, rwindex)
    begin
        if rindex = rwindex then
            empty <= '1';
        else
            empty <= '0';
        end if;
    end process;

    -- write process
    process(wreset(1), wclk, windex)
        variable i : std_logic_vector(depth_shift - 1 downto 0);
    begin
        -- write index without MSB
        i := windex(depth_shift - 1 downto 0);

        if wreset(1) = '1' then
            data <= (others => (others => '0'));
        elsif rising_edge(wclk) then
            if wen = '1' then
                -- do the actual read
                data(to_integer(unsigned(i))) <= wdata;
            end if;
        end if;
    end process;

    -- fifo full process
    process(windex, wrindex)
    begin
        -- Full if the MSB's of the indexes are different but the other
        -- bits are the same.
        if windex = (wrindex xor index_msb) then
            full <= '1';
        else
            full <= '0';
        end if;
    end process;
end behavioral;
