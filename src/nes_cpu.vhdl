library ieee;
use ieee.std_logic_1164.all;

use ieee.numeric_std.all;

entity nes_cpu is
    port (
        clk : in std_logic;
        run : in std_logic := '1';
        areset : in std_logic;
        addr : out std_logic_vector(15 downto 0);
        din : in std_logic_vector(7 downto 0);
        dout : out std_logic_vector(7 downto 0);
        we : out std_logic;
        sync : out std_logic;

        pc : inout std_logic_vector(15 downto 0);
        ir : inout std_logic_vector(7 downto 0);
        p : inout std_logic_vector(7 downto 0);
        a : inout std_logic_vector(7 downto 0);
        s : inout std_logic_vector(7 downto 0);
        x : inout std_logic_vector(7 downto 0);
        y : inout std_logic_vector(7 downto 0);
        step : inout std_logic_vector(7 downto 0)
    );
end nes_cpu;

architecture structural of nes_cpu is
    -- All the logic that is executed by the cpu is derived from the current
    -- instruction and the current cycle of that instruction (the step count).
    -- The behavior in a single cycle is fully defined by a set of defined
    -- actions. Mutually exclusive actions are grouped into an enum. They are
    -- commonly grouped by result register. For example, the PC register:
    --   - increment PC
    --   - Load DIN into PCL
    --   - ...
    -- The set of action enums are aggregrated into the step_act record.
    -- The step_act record defines all the actions to be performed in a single
    -- cycle.
    --
    -- Finally, a lookup table is formed that specifies step_act's for each
    -- cycle of each instruction. Helper functions are used to reduce
    -- repetition in specifying instructions.

    -- action enums
    type step_count_act is (ACT_INC, ACT_RESET, ACT_RESET_NO_BRANCH, ACT_SKIP_NO_C);
    type pc_act is (ACT_HOLD, ACT_INC, ACT_L_DIN, ACT_H_DIN, ACT_DIN_TEMP, ACT_L_ALU);
    type addr_act is (ACT_DC, ACT_PC, ACT_S, ACT_FFFC, ACT_FFFD, ACT_ZP_TEMP, ACT_AD);
    type dout_act is (ACT_NONE, ACT_A, ACT_X, ACT_Y, ACT_PCL, ACT_PCH, ACT_P, ACT_ALU, ACT_TEMP);
    type s_act is (ACT_HOLD, ACT_DEC, ACT_INC, ACT_ALU);
    type p_i_act is (ACT_HOLD, ACT_SET, ACT_CLR, ACT_DIN2);
    type p_c_act is (ACT_HOLD, ACT_SET, ACT_CLR, ACT_ALU, ACT_DIN0);
    type p_z_act is (ACT_HOLD, ACT_ALU, ACT_DIN1);
    type p_n_act is (ACT_HOLD, ACT_ALU, ACT_DIN7);
    type p_v_act is (ACT_HOLD, ACT_ALU, ACT_CLR, ACT_DIN6);
    type p_d_act is (ACT_HOLD, ACT_SET, ACT_CLR, ACT_DIN3);
    type a_act is (ACT_HOLD, ACT_ALU);
    type x_act is (ACT_HOLD, ACT_ALU);
    type y_act is (ACT_HOLD, ACT_ALU);
    type temp_act is (ACT_DC, ACT_HOLD, ACT_DIN, ACT_ALU, ACT_ALU_COUT);
    type adl_act is (ACT_DC, ACT_HOLD, ACT_DIN, ACT_ALU);
    type adh_act is (ACT_DC, ACT_HOLD, ACT_DIN, ACT_00, ACT_ALU);
    type page_cross_act is (ACT_DC, ACT_HOLD, ACT_ALU_C);
    type alu_op_act is (ACT_DC, ACT_ADC, ACT_AND, ACT_CMP, ACT_SBC, ACT_OR, ACT_XOR, ACT_LSR);
    type alu_a_act is (ACT_DC, ACT_DIN, ACT_PCL, ACT_A, ACT_S, ACT_X, ACT_Y, ACT_TEMP, ACT_ADL, ACT_ADH);
    type alu_b_act is (ACT_DC, ACT_A, ACT_X, ACT_Y, ACT_00, ACT_TEMP, ACT_DIN);
    type alu_cin_act is (ACT_DC, ACT_0, ACT_1, ACT_P_C, ACT_PX);

    -- specifies the actions for a step (a cycle of an instruction)
    type step_act is record
        step_count : step_count_act;
        pc : pc_act;
        addr : addr_act;
        dout : dout_act;
        s : s_act;
        p_i : p_i_act;
        p_c : p_c_act;
        p_z : p_z_act;
        p_n : p_n_act;
        p_v : p_v_act;
        p_d : p_d_act;
        temp : temp_act;
        adl : adl_act;
        adh : adh_act;
        page_cross : page_cross_act;
        alu_op : alu_op_act;
        alu_a : alu_a_act;
        alu_b : alu_b_act;
        alu_cin : alu_cin_act;
        a : a_act;
        x : x_act;
        y : y_act;
    end record step_act;

    -- Instruction sequence, the set of step_act's for a complete instruction.
    -- Note, step 0 is the fetch cycle, which is equal for all instructions.
    type instr_seq is array(1 to 7) of step_act;

    -- All the instructions
    type decode_rom_type is array(0 to 255) of instr_seq;

    -- default step act.
    -- A default avoids to have to define every action for each cycle.
    constant DEFAULT_STEP_ACT : step_act := (
        step_count => ACT_INC,
        pc => ACT_HOLD,
        addr => ACT_DC,
        s => ACT_HOLD,
        p_i => ACT_HOLD,
        p_c => ACT_HOLD,
        p_z => ACT_HOLD,
        p_n => ACT_HOLD,
        p_v => ACT_HOLD,
        p_d => ACT_HOLD,
        a => ACT_HOLD,
        x => ACT_HOLD,
        y => ACT_HOLD,
        temp => ACT_DC,
        adl => ACT_DC,
        adh => ACT_DC,
        page_cross => ACT_DC,
        dout => ACT_NONE,
        alu_op => ACT_DC,
        alu_a => ACT_DC,
        alu_b => ACT_DC,
        alu_cin => ACT_DC);

    -- default instr_seq.
    constant DEFAULT_INSTR_SEQ : instr_seq := (
        others => DEFAULT_STEP_ACT);

    -- fetch step act
    function fetch
        return step_act is

        variable s : step_act := DEFAULT_STEP_ACT;
    begin
        s.addr := ACT_PC;
        s.pc := ACT_INC;
        return s;
    end function;

    -- helper functions
    function push(
            dout : dout_act;
            s : step_act := DEFAULT_STEP_ACT)
        return step_act is

        variable i : step_act := s;
    begin
        i.addr := ACT_S;
        i.dout := dout;
        i.s := ACT_DEC;
        return i;
    end function;

    -- Load P register from stack
    function s_to_p(
            s : step_act := DEFAULT_STEP_ACT)
        return step_act is

        variable i : step_act := s;
    begin
        i.addr := ACT_S;
        i.p_n := ACT_DIN7;
        i.p_v := ACT_DIN6;
        i.p_d := ACT_DIN3;
        i.p_i := ACT_DIN2;
        i.p_z := ACT_DIN1;
        i.p_c := ACT_DIN0;
        return i;
    end function;

    -- Update P register with ALU results
    function alu_to_p(
            s : step_act := DEFAULT_STEP_ACT;
            c : boolean := false;
            v : boolean := false)
        return step_act is

        variable i : step_act := s;
    begin
        i.p_n := ACT_ALU;
        i.p_z := ACT_ALU;

        if c then i.p_c := ACT_ALU; end if;
        if v then i.p_v := ACT_ALU; end if;
        return i;
    end function;

    -- run an input through the alu
    -- this yields status flags and also the
    -- value can be loaded from the alu output like other
    -- alu ops. This reduces the number of datapaths
    -- as the alu becomes an intersection
    function eval_alu(
            a : alu_a_act;
            s : step_act := DEFAULT_STEP_ACT)
        return step_act is

        variable i : step_act := s;
    begin
        i.alu_a := a;
        i.alu_b := ACT_00;
        i.alu_cin := ACT_0;
        i.alu_op := ACT_ADC;
        i := alu_to_p(i);
        return i;
    end function;

    -- reset the step counter after x cycles
    -- TODO: set don't cares for steps after x cycles
    function num_cycles(x : natural;
                        s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
    begin
        for k in s'low to x-2 loop
            i(k).step_count := ACT_INC;
        end loop;
        i(x-1).step_count := ACT_RESET;
        return i;
    end function;

    -- derive the number of cycles
    function get_num_cycles(s : instr_seq := DEFAULT_INSTR_SEQ) return natural is
        -- start at 1 to account for fetch cycle which is not part of instr_seq.
        variable n : natural := 1;
    begin
        for k in s'low to s'high loop
            n := n + 1;
            exit when s(k).step_count = ACT_RESET;
        end loop;
        return n;
    end function;

    -- the interrupt sequence
    function brk_seq(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is

        variable i : instr_seq := s;
    begin
        i(1).addr := ACT_PC;
        i(1).pc := ACT_INC;

        i(2).addr := ACT_S;
        i(2).s := ACT_DEC;

        i(3).addr := ACT_S;
        i(3).s := ACT_DEC;

        i(4).addr := ACT_S;
        i(4).s := ACT_DEC;

        i(5).addr := ACT_FFFC;
        i(5).pc := ACT_L_DIN;

        i(6).addr := ACT_FFFD;
        i(6).pc := ACT_H_DIN;
        i(6).p_i := ACT_SET;

        i(6).step_count := ACT_RESET;
        return i;
    end function;

    function rti(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is

        variable i : instr_seq := s;
    begin
        i := num_cycles(6, i);

        i(2).s := ACT_INC;

        -- fetch P
        i(3) := s_to_p(i(3));
        i(3).s := ACT_INC;

        -- fetch PCL
        i(4).addr := ACT_S;
        i(4).temp := ACT_DIN;
        i(4).s := ACT_INC;

        -- fetch PCH
        i(5).addr := ACT_S;
        i(5).pc := ACT_DIN_TEMP;

        return i;
    end function;

    function jmp_abs(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is

        variable i : instr_seq := s;
    begin
        i(1).addr := ACT_PC;
        i(1).pc := ACT_INC;
        i(1).temp := ACT_DIN;

        i(2).addr := ACT_PC;
        i(2).pc := ACT_DIN_TEMP;
        i(2).step_count := ACT_RESET;

        return i;
    end function;

    function jmp_i_abs(s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(5, i);

        i(1).addr := ACT_PC;
        i(1).pc := ACT_INC;
        i(1).adl := ACT_DIN;

        i(2).addr := ACT_PC;
        i(2).adh := ACT_DIN;
        i(2).adl := ACT_HOLD;

        i(3).adh := ACT_HOLD;

        i(3).addr := ACT_AD;
        i(3).temp := ACT_DIN;

        i(3).alu_a := ACT_ADL;
        i(3).alu_b := ACT_00;
        i(3).alu_op := ACT_ADC;
        i(3).alu_cin := ACT_1;
        i(3).adl := ACT_ALU;

        i(4).addr := ACT_AD;
        i(4).pc := ACT_DIN_TEMP;

        return i;
    end function;

    function jsr(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is

        variable i : instr_seq := s;
    begin
        i(1).addr := ACT_PC;
        i(1).pc := ACT_INC;
        i(1).temp := ACT_DIN;

        i(2).temp := ACT_HOLD;

        i(3) := push(ACT_PCH);
        i(3).temp := ACT_HOLD;

        i(4) := push(ACT_PCL);
        i(4).temp := ACT_HOLD;

        i(5).addr := ACT_PC;
        i(5).pc := ACT_DIN_TEMP;
        i(5).step_count := ACT_RESET;

        return i;
    end function;

    function rts(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is

        variable i : instr_seq := s;
    begin
        i := num_cycles(6, i);

        i(2).s := ACT_INC;

        i(3).addr := ACT_S;
        i(3).temp := ACT_DIN;
        i(3).s := ACT_INC;

        i(4).addr := ACT_S;
        i(4).pc := ACT_DIN_TEMP;

        i(5).pc := ACT_INC;

        return i;
    end function;

    -- creates a instr_seq which resets the step counter after 2 cycles
    -- if the branch condition is false, otherwise step counter is reset
    -- in the 3rd cycle.
    function branch(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is

        variable i : instr_seq := s;
    begin
        i(1).pc := ACT_INC;
        i(1).addr := ACT_PC;
        i(1).temp := ACT_DIN;
        i(1).step_count := ACT_RESET_NO_BRANCH;

        i(2).step_count := ACT_RESET;
        i(2).alu_a := ACT_PCL;
        i(2).alu_b := ACT_TEMP;
        i(2).alu_cin := ACT_0;
        i(2).alu_op := ACT_ADC;
        i(2).pc := ACT_L_ALU;
        return i;
    end function;

    -- clear or set C flag
    function act_p_c(
            a : p_c_act;
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(2, i);
        i(1).p_c := a;
        return i;
    end function;

    -- clear or set D flag
    function act_p_d(
            a : p_d_act;
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(2, i);
        i(1).p_d := a;
        return i;
    end function;

    -- clear or set I flag
    function act_p_i(
            a : p_i_act;
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(2, i);
        i(1).p_i := a;
        return i;
    end function;

    -- clear V flag
    function clv(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(2, i);
        i(1).p_v := ACT_CLR;
        return i;
    end function;

    -- generic function for pha and php
    function phx(
            d : dout_act;
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(3, i);
        i(2) := push(d, i(2));
        return i;
    end function;

    function pla(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(4, i);

        i(2).s := ACT_INC;

        -- run through ALU
        i(3).addr := ACT_S;
        i(3).alu_a := ACT_DIN;
        i(3).alu_b := ACT_00;
        i(3).alu_cin := ACT_0;
        i(3).alu_op := ACT_ADC;
        i(3).p_z := ACT_ALU;
        i(3).p_n := ACT_ALU;
        i(3).a := ACT_ALU;
        return i;
    end function;

    function plp(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(4, i);

        i(2).s := ACT_INC;
        i(3) := s_to_p(i(3));
        return i;
    end function;

    -- Transfer instructions
    function transfer_common(
            from : alu_a_act;
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(2, i);
        i(1) := eval_alu(from, i(1));
        return i;
    end function;

    function tax(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := transfer_common(ACT_A, i);
        i(1).x := ACT_ALU;
        return i;
    end function;

    function tay(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := transfer_common(ACT_A, i);
        i(1).y := ACT_ALU;
        return i;
    end function;

    function txa(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := transfer_common(ACT_X, i);
        i(1).a := ACT_ALU;
        return i;
    end function;

    function tya(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := transfer_common(ACT_Y, i);
        i(1).a := ACT_ALU;
        return i;
    end function;

    function tsx(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := transfer_common(ACT_S, i);
        i(1).x := ACT_ALU;
        return i;
    end function;

    function txs(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := transfer_common(ACT_X, i);
        i(1).s := ACT_ALU;

        -- txs is the only transfer function that does not
        -- set the z and n flags. Override actions set by
        -- transfer_common
        i(1).p_z := ACT_HOLD;
        i(1).p_n := ACT_HOLD;
        return i;
    end function;

    -- immediate page helper
    --  * 2 cycles
    --  * output imm address to addr on second cycle
    function addr_imm(s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(2, i);
        i(1).pc := ACT_INC;
        i(1).addr := ACT_PC;
        return i;
    end function;

    -- zero page helper
    --  * 3 cycles
    --  * fetch address into temp
    --  * output zp address to addr on 3rd cycle
    function addr_zp(
            s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(3, i);
        i(1).addr := ACT_PC;
        i(1).adh := ACT_00;
        i(1).adl := ACT_DIN;
        i(1).pc := ACT_INC;
        i(2).addr := ACT_AD;
        return i;
    end function;

    -- ZP index helper
    function addr_zp_indexed(
            o : alu_b_act;
            s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(4, i);

        -- fetch ZP address
        i(1).addr := ACT_PC;
        i(1).pc := ACT_INC;
        i(1).temp := ACT_DIN;

        -- index with X
        i(2).alu_a := ACT_TEMP;
        i(2).alu_b := o;
        i(2).alu_op := ACT_ADC;
        i(2).alu_cin := ACT_0;
        i(2).adl := ACT_ALU;
        i(2).adh := ACT_00;

        -- Fetch operand
        i(3).addr := ACT_AD;
        return i;
    end function;


    -- common code for an Absolute instruction
    --  * set #cycles to 4
    --  * fetch the absolute address into adl/adh
    --  * output address in the 4th cycle.
    function addr_abs(s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is

        variable i : instr_seq := s;
    begin
        i := num_cycles(4, i);

        i(1).addr := ACT_PC;
        i(1).pc := ACT_INC;
        i(1).adl := ACT_DIN;

        i(2).addr := ACT_PC;
        i(2).pc := ACT_INC;
        i(2).adh := ACT_DIN;
        i(2).adl := ACT_HOLD;

        i(3).addr := ACT_AD;

        return i;
    end function;

    function addr_abs_indexed(
            o : alu_b_act;
            always_page_cross : boolean := false;
            s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(5, i);

        -- fetch ADL into temp
        i(1).addr := ACT_PC;
        i(1).pc := ACT_INC;
        i(1).temp := ACT_DIN;

        -- index ADL with Y
        i(2).alu_a := ACT_TEMP;
        i(2).alu_b := o;
        i(2).alu_op := ACT_ADC;
        i(2).alu_cin := ACT_0;
        i(2).adl := ACT_ALU;
        i(2).page_cross := ACT_ALU_C;

        -- Fetch ADH into temp
        i(2).addr := ACT_PC;
        i(2).pc := ACT_INC;
        i(2).adh := ACT_DIN;

        if not always_page_cross then
            i(2).step_count := ACT_SKIP_NO_C;
        end if;

        -- increment ADH (page cross)
        i(3).alu_op := ACT_ADC;
        i(3).alu_a := ACT_ADH;
        i(3).alu_b := ACT_00;
        i(3).alu_cin := ACT_PX;
        i(3).adh := ACT_ALU;
        i(3).adl := ACT_HOLD;

        -- fetch operand
        i(4).addr := ACT_AD;

        return i;
    end function;

    -- (Indirect, X) instructions
    --  * 6 cycles
    --  * absolute address is latched into ad
    --  * and is output on addr in 6th cycle
    function addr_indexed_indirect(s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(6, i);

        -- fetch ZP address
        i(1).addr := ACT_PC;
        i(1).pc := ACT_INC;
        i(1).temp := ACT_DIN;

        -- index with X
        i(2).alu_a := ACT_TEMP;
        i(2).alu_b := ACT_X;
        i(2).alu_op := ACT_ADC;
        i(2).alu_cin := ACT_0;
        i(2).temp := ACT_ALU;

        -- Fetch adl, and increment temp
        i(3).addr := ACT_ZP_TEMP;
        i(3).adl := ACT_DIN;
        -- and increment temp
        i(3).alu_a := ACT_TEMP;
        i(3).alu_b := ACT_00;
        i(3).alu_op := ACT_ADC;
        i(3).alu_cin := ACT_1;
        i(3).temp := ACT_ALU;

        -- Fetch adh
        i(4).adl := ACT_HOLD;
        i(4).addr := ACT_ZP_TEMP;
        i(4).adh := ACT_DIN;

        -- Fetch operand
        i(5).addr := ACT_AD;

        return i;
    end function;

    function addr_indirect_indexed(
            always_page_cross : boolean := false;
            s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(6, i);

        -- fetch ZP address
        i(1).addr := ACT_PC;
        i(1).pc := ACT_INC;
        i(1).temp := ACT_DIN;

        -- Fetch adl, and increment temp
        i(2).addr := ACT_ZP_TEMP;
        i(2).adl := ACT_DIN;
        -- and increment temp
        i(2).alu_a := ACT_TEMP;
        i(2).alu_b := ACT_00;
        i(2).alu_op := ACT_ADC;
        i(2).alu_cin := ACT_1;
        i(2).temp := ACT_ALU;

        -- Fetch adh and index adl with Y
        i(3).addr := ACT_ZP_TEMP;
        i(3).adh := ACT_DIN;

        i(3).alu_a := ACT_ADL;
        i(3).alu_b := ACT_Y;
        i(3).alu_op := ACT_ADC;
        i(3).alu_cin := ACT_0;
        i(3).adl := ACT_ALU;
        i(3).temp := ACT_ALU_COUT;

        if not always_page_cross then
            -- if alu_cout is '0' then skip next step because
            -- adh must not be incremented.
            -- STA and the RMW instructions always assume this a
            -- page cross occurs, hence the if statement.
            i(3).step_count := ACT_SKIP_NO_C;
        end if;

        -- increment ADH
        i(4).adl := ACT_HOLD;
        i(4).alu_a := ACT_ADH;
        i(4).alu_b := ACT_TEMP;
        i(4).alu_op := ACT_ADC;
        i(4).alu_cin := ACT_0;
        i(4).adh := ACT_ALU;

        -- fetch operand
        i(5).addr := ACT_AD;

        return i;
    end function;

    -- lda in last step (step_count must be set)
    function lda(s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
        variable n : natural := get_num_cycles(s) - 1;
    begin
        i(n) := eval_alu(ACT_DIN, i(n));
        i(n).a := ACT_ALU;
        return i;
    end function;

    -- ldx in last step (step_count must be set)
    function ldx(s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
        variable n : natural := get_num_cycles(s) - 1;
    begin
        i(n) := eval_alu(ACT_DIN, i(n));
        i(n).x := ACT_ALU;
        return i;
    end function;

    -- ldy in last step (step_count must be set)
    function ldy(s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
        variable n : natural := get_num_cycles(s) - 1;
    begin
        i(n) := eval_alu(ACT_DIN, i(n));
        i(n).y := ACT_ALU;
        return i;
    end function;

    -- st in last step (step_count must be set)
    function st(dout : dout_act;
                s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
        variable n : natural := get_num_cycles(s) - 1;
    begin
        i(n).dout := dout;
        return i;
    end function;

    function bit_op(s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
        variable n : natural := get_num_cycles(s) - 1;
    begin
        i(n).alu_a := ACT_A;
        i(n).alu_b := ACT_DIN;
        i(n).alu_op := ACT_AND;

        i(n).p_z := ACT_ALU;
        i(n).p_n := ACT_DIN7;
        i(n).p_v := ACT_DIN6;

        return i;
    end function;

    function alu(o : alu_op_act;
                    s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
        variable n : natural := get_num_cycles(s) - 1;
    begin
        i(n).alu_a := ACT_A;
        i(n).alu_b := ACT_DIN;
        i(n).alu_op := o;

        i(n).a := ACT_ALU;
        i(n) := alu_to_p(i(n));

        return i;
    end function;

    function arith(o : alu_op_act;
                   s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
        variable n : natural := get_num_cycles(s) - 1;
    begin
        i := alu(o, i);

        i(n).alu_cin := ACT_P_C;
        i(n) := alu_to_p(i(n), c => true, v => true);

        return i;
    end function;

    function cmp(a : alu_a_act;
                 s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable n : natural := get_num_cycles(s) - 1;
        variable i : instr_seq := s;
    begin
        i(n).alu_a := a;
        i(n).alu_b := ACT_DIN;
        i(n).alu_cin := ACT_P_C;
        i(n).alu_op := ACT_CMP;

        i(n) := alu_to_p(i(n), c => true);

        return i;
    end function;

    function inc_common(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(2, i);

        i(1).alu_b := ACT_00;
        i(1).alu_cin := ACT_1;
        i(1).alu_op := ACT_ADC;

        i(1) := alu_to_p(i(1));

        return i;
    end function;

    function iny(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := inc_common(i);
        i(1).alu_a := ACT_Y;
        i(1).y := ACT_ALU;
        return i;
    end function;

    function inx(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := inc_common(i);
        i(1).alu_a := ACT_X;
        i(1).x := ACT_ALU;
        return i;
    end function;

    function dec_common(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(2, i);

        i(1).alu_b := ACT_00;
        i(1).alu_cin := ACT_0;
        i(1).alu_op := ACT_SBC;

        i(1) := alu_to_p(i(1));

        return i;
    end function;

    function dey(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := dec_common(i);
        i(1).alu_a := ACT_Y;
        i(1).y := ACT_ALU;
        return i;
    end function;

    function dex(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := dec_common(i);
        i(1).alu_a := ACT_X;
        i(1).x := ACT_ALU;
        return i;
    end function;

    function lsr_a(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(2, i);

        i(1).alu_op := ACT_LSR;
        i(1).alu_a := ACT_A;
        i(1).alu_cin := ACT_0;

        -- p_n is always 0 in this case, ACT_ALU uses probably less resources
        i(1) := alu_to_p(i(1), c => true);
        i(1).a := ACT_ALU;
        return i;
    end function;

    function asl_a(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(2, i);

        i(1).alu_op := ACT_ADC;
        i(1).alu_a := ACT_A;
        i(1).alu_b := ACT_A;
        i(1).alu_cin := ACT_0;

        -- p_n is always 0 in this case, ACT_ALU uses probably less resources
        -- than introducing a new ACT_0
        i(1) := alu_to_p(i(1), c => true);
        i(1).a := ACT_ALU;
        return i;
    end function;

    function ror_a(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(2, i);

        i(1).alu_op := ACT_LSR;
        i(1).alu_a := ACT_A;
        i(1).alu_cin := ACT_P_C;

        i(1) := alu_to_p(i(1), c => true);
        i(1).a := ACT_ALU;
        return i;
    end function;

    function rol_a(
            s : instr_seq := DEFAULT_INSTR_SEQ)
        return instr_seq is
        variable i : instr_seq := s;
    begin
        i := num_cycles(2, i);

        i(1).alu_op := ACT_ADC;
        i(1).alu_a := ACT_A;
        i(1).alu_b := ACT_A;
        i(1).alu_cin := ACT_P_C;

        i(1) := alu_to_p(i(1), c => true);
        i(1).a := ACT_ALU;
        return i;
    end function;


    -- Add additional RMW cycles
    -- Use like rmw(addr_zp(...)); to modify
    function rmw(s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
        variable n : natural := get_num_cycles(s);
    begin
        -- add two more cycles
        i := num_cycles(n + 2, i);

        -- do fetch and hold AD for following cycles
        i(n-1).temp := ACT_DIN;
        i(n-1).adl := ACT_HOLD;
        i(n-1).adh := ACT_HOLD;

        -- capture result of operation in temp
        i(n).temp := ACT_ALU;
        i(n).adl := ACT_HOLD;
        i(n).adh := ACT_HOLD;

        -- write back result
        i(n+1).addr := ACT_AD;
        i(n+1).dout := ACT_TEMP;
        return i;
    end function;

    function lsr(s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
        variable n : natural := get_num_cycles(s);
    begin
        i(n-2).alu_op := ACT_LSR;
        i(n-2).alu_a := ACT_TEMP;
        i(n-2).alu_cin := ACT_0;
        i(n-2) := alu_to_p(i(n-2), c => true);
        return i;
    end function;

    function asl(s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
        variable n : natural := get_num_cycles(s);
    begin
        i(n-2).alu_op := ACT_ADC;
        i(n-2).alu_a := ACT_A;
        i(n-2).alu_b := ACT_A;
        i(n-2).alu_cin := ACT_0;
        i(n-2) := alu_to_p(i(n-2), c => true);
        return i;
    end function;

    function rotr(s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
        variable n : natural := get_num_cycles(s);
    begin
        i(n-2).alu_op := ACT_LSR;
        i(n-2).alu_a := ACT_A;
        i(n-2).alu_cin := ACT_P_C;
        i(n-2) := alu_to_p(i(n-2), c => true);
        return i;
    end function;

    function rotl(s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
        variable n : natural := get_num_cycles(s);
    begin
        i(n-2).alu_op := ACT_ADC;
        i(n-2).alu_a := ACT_A;
        i(n-2).alu_b := ACT_A;
        i(n-2).alu_cin := ACT_P_C;
        i(n-2) := alu_to_p(i(n-2), c => true);
        return i;
    end function;

    function inc(s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
        variable n : natural := get_num_cycles(s);
    begin
        i(n-2).alu_op := ACT_ADC;
        i(n-2).alu_a := ACT_TEMP;
        i(n-2).alu_b := ACT_00;
        i(n-2).alu_cin := ACT_1;
        i(n-2) := alu_to_p(i(n-2));
        return i;
    end function;

    function dec(s : instr_seq := DEFAULT_INSTR_SEQ) return instr_seq is
        variable i : instr_seq := s;
        variable n : natural := get_num_cycles(s);
    begin
        i(n-2).alu_op := ACT_SBC;
        i(n-2).alu_a := ACT_TEMP;
        i(n-2).alu_b := ACT_00;
        i(n-2).alu_cin := ACT_0;
        i(n-2) := alu_to_p(i(n-2));
        return i;
    end function;

    -- The actual decode rom
    constant decode_rom : decode_rom_type := (
        16#00# => -- BRK
            brk_seq,
        16#01# => -- ORA (Indirect, X)
            alu(ACT_OR, addr_indexed_indirect),
        16#02# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#03# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#04# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#05# => -- ORA Zero Page
            alu(ACT_OR, addr_zp),
        16#06# => -- ASL Zero Page
            asl(rmw(addr_zp)),
        16#07# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#08# => -- PHP
            phx(ACT_P),
        16#09# => -- ORA Immediate
            alu(ACT_OR, addr_imm),
        16#0a# => -- ASL Accumulator
            asl_a,
        16#0b# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#0c# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#0d# => -- ORA Absolute
            alu(ACT_OR, addr_abs),
        16#0e# => -- ASL Absolute
            asl(rmw(addr_abs)),
        16#0f# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#10# => -- BPL
            branch,
        16#11# => -- ORA (Indirect), Y
            alu(ACT_OR, addr_indirect_indexed),
        16#12# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#13# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#14# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#15# => -- ORA Zero Page, X
            alu(ACT_OR, addr_zp_indexed(ACT_X)),
        16#16# => -- ASL Zero Page, X
            asl(rmw(addr_zp_indexed(ACT_X))),
        16#17# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#18# => -- CLC
            act_p_c(ACT_CLR),
        16#19# => -- ORA Absolute, Y
            alu(ACT_OR, addr_abs_indexed(ACT_Y)),
        16#1a# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#1b# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#1c# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#1d# => -- ORA Absolute, X
            alu(ACT_OR, addr_abs_indexed(ACT_X)),
        16#1e# => -- ASL Absolute, X
            asl(rmw(addr_abs_indexed(ACT_X, always_page_cross => true))),
        16#1f# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#20# => -- JSR
            jsr,
        16#21# => -- AND (Indirect, X)
            alu(ACT_AND, addr_indexed_indirect),
        16#22# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#23# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#24# => -- BIT Zero Page
            bit_op(addr_zp),
        16#25# => -- AND Zero Page
            alu(ACT_AND, addr_zp),
        16#26# => -- ROL Zero Page
            rotl(rmw(addr_zp)),
        16#27# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#28# => -- PLP
            plp,
        16#29# => -- AND Immediate
            alu(ACT_AND, addr_imm),
        16#2a# => -- ROL Accumulator
            rol_a,
        16#2b# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#2c# => -- BIT Absolute
            bit_op(addr_abs),
        16#2d# => -- AND Absolute
            alu(ACT_AND, addr_abs),
        16#2e# => -- ROL Absolute
            rotl(rmw(addr_abs)),
        16#2f# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#30# => -- BMI
            branch,
        16#31# => -- AND (Indirect), Y
            alu(ACT_AND, addr_indirect_indexed),
        16#32# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#33# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#34# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#35# => -- AND Zero Page, X
            alu(ACT_AND, addr_zp_indexed(ACT_X)),
        16#36# => -- ROL Zero Page, X
            rotl(rmw(addr_zp_indexed(ACT_X))),
        16#37# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#38# => -- SEC
            act_p_c(ACT_SET),
        16#39# => -- AND Absolute, Y
            alu(ACT_AND, addr_abs_indexed(ACT_Y)),
        16#3a# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#3b# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#3c# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#3d# => -- AND Absolute, X
            alu(ACT_AND, addr_abs_indexed(ACT_X)),
        16#3e# => -- ROL Absolute, X
            rotl(rmw(addr_abs_indexed(ACT_X, always_page_cross => true))),
        16#3f# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#40# => -- RTI
            rti,
        16#41# => -- EOR (Indirect, X)
            alu(ACT_XOR, addr_indexed_indirect),
        16#42# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#43# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#44# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#45# => -- EOR Zero Page
            alu(ACT_XOR, addr_zp),
        16#46# => -- LSR Zero Page
            lsr(rmw(addr_zp)),
        16#47# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#48# => -- PHA
            phx(ACT_A),
        16#49# => -- EOR Immediate
            alu(ACT_XOR, addr_imm),
        16#4a# => -- LSR Accumulator
            lsr_a,
        16#4b# => -- Future Expansion
            (others => DEFAULT_STEP_ACT),
        16#4c# => -- JMP Absolute
            jmp_abs,
        16#4d# => -- EOR Absolute
            alu(ACT_XOR, addr_abs),
        16#4e# => -- LSR Absolute
            lsr(rmw(addr_abs)),
        16#4f# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#50# => -- BVC
            branch,
        16#51# => -- EOR (Indirect), Y
            alu(ACT_XOR, addr_indirect_indexed),
        16#52# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#53# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#54# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#55# => -- EOR Zero Page, X
            alu(ACT_XOR, addr_zp_indexed(ACT_X)),
        16#56# => -- LSR Zero Page, X
            lsr(rmw(addr_zp_indexed(ACT_X))),
        16#57# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#58# => -- CLI
            act_p_i(ACT_CLR),
        16#59# => -- EOR Absolute, Y
            alu(ACT_XOR, addr_abs_indexed(ACT_Y)),
        16#5a# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#5b# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#5c# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#5d# => -- EOR Absolute, X
            alu(ACT_XOR, addr_abs_indexed(ACT_X)),
        16#5e# => -- LSR Absolute, X
            lsr(rmw(addr_abs_indexed(ACT_X, always_page_cross => true))),
        16#5f# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#60# => -- RTS
            rts,
        16#61# => -- ADC (Indirect, X)
            arith(ACT_ADC, addr_indexed_indirect),
        16#62# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#63# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#64# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#65# => -- ADC Zero Page
            arith(ACT_ADC, addr_zp),
        16#66# => -- ROR Zero Page
            rotr(rmw(addr_zp)),
        16#67# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#68# => -- PLA
            pla,
        16#69# => -- ADC Immediate
            arith(ACT_ADC, addr_imm),
        16#6a# => -- ROR Accumulator
            ror_a,
        16#6b# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#6c# => -- JMP Indirect
            jmp_i_abs,
        16#6d# => -- ADC Absolute
            arith(ACT_ADC, addr_abs),
        16#6e# => -- ROR Absolute
            rotr(rmw(addr_abs)),
        16#6f# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#70# => -- BVS
            branch,
        16#71# => -- ADC (Indirect), Y
            arith(ACT_ADC, addr_indirect_indexed),
        16#72# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#73# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#74# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#75# => -- ADC Zero Page, X
            arith(ACT_ADC, addr_zp_indexed(ACT_X)),
        16#76# => -- ROR Zero Page, X
            rotr(rmw(addr_zp_indexed(ACT_X))),
        16#77# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#78# => -- SEI
            act_p_i(ACT_SET),
        16#79# => -- ADC Absolute, Y
            arith(ACT_ADC, addr_abs_indexed(ACT_Y)),
        16#7a# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#7b# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#7c# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#7d# => -- ADC Absolute, X
            arith(ACT_ADC, addr_abs_indexed(ACT_X)),
        16#7e# => -- ROR Absolute, X
            rotr(rmw(addr_abs_indexed(ACT_X, always_page_cross => true))),
        16#7f# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#80# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#81# => -- STA (Indirect, X)
            st(ACT_A, addr_indexed_indirect),
        16#82# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#83# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#84# => -- STY Zero Page
            st(ACT_Y, addr_zp),
        16#85# => -- STA Zero Page
            st(ACT_A, addr_zp),
        16#86# => -- STX Zero Page
            st(ACT_X, addr_zp),
        16#87# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#88# => -- DEY
            dey,
        16#89# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#8a# => -- TXA
            txa,
        16#8b# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#8c# => -- STY Absolute
            st(ACT_Y, addr_abs),
        16#8d# => -- STA Absolute
            st(ACT_A, addr_abs),
        16#8e# => -- STX Absolute
            st(ACT_X, addr_abs),
        16#8f# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#90# => -- BCC
            branch,
        16#91# => -- STA (Indirect), Y
            st(ACT_A, addr_indirect_indexed(always_page_cross => true)),
        16#92# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#93# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#94# => -- STY Zero Page, X
            st(ACT_Y, addr_zp_indexed(ACT_X)),
        16#95# => -- STA Zero Page, X
            st(ACT_A, addr_zp_indexed(ACT_X)),
        16#96# => -- STX Zero Page, Y
            st(ACT_X, addr_zp_indexed(ACT_Y)),
        16#97# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#98# => -- TYA
            tya,
        16#99# => -- STA Absolute, Y
            st(ACT_A, addr_abs_indexed(ACT_Y, always_page_cross => true)),
        16#9a# => -- TXS
            txs,
        16#9b# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#9c# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#9d# => -- STA Absolute, X
            st(ACT_A, addr_abs_indexed(ACT_X, always_page_cross => true)),
        16#9e# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#9f# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#a0# => -- LDY Immediate
            ldy(addr_imm),
        16#a1# => -- LDA (Indirect, X)
            lda(addr_indexed_indirect),
        16#a2# => -- LDX Immediate
            ldx(addr_imm),
        16#a3# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#a4# => -- LDY Zero Page
            ldy(addr_zp),
        16#a5# => -- LDA Zero Page
            lda(addr_zp),
        16#a6# => -- LDX Zero Page
            ldx(addr_zp),
        16#a7# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#a8# => -- TAY
            tay,
        16#a9# => -- LDA Immediate
            lda(addr_imm),
        16#aa# => -- TAX
            tax,
        16#ab#=> -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#ac# => -- LDY Absolute
            ldy(addr_abs),
        16#ad# => -- LDA Absolute
            lda(addr_abs),
        16#ae# => -- LDX Absolute
            ldx(addr_abs),
        16#af# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#b0# => -- BCS
            branch,
        16#b1# => -- LDA (Indirect), Y
            lda(addr_indirect_indexed),
        16#b2# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#b3# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#b4# => -- LDY Zero Page, X
            ldy(addr_zp_indexed(ACT_X)),
        16#b5# => -- LDA Zero Page, X
            lda(addr_zp_indexed(ACT_X)),
        16#b6# => -- LDX Zero Page, Y
            ldx(addr_zp_indexed(ACT_Y)),
        16#b7# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#b8# => -- CLV
            clv,
        16#b9# => -- LDA Absolute, Y
            lda(addr_abs_indexed(ACT_Y)),
        16#ba# => -- TSX
            tsx,
        16#bb# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#bc# => -- LDY Absolute, X
            ldy(addr_abs_indexed(ACT_X)),
        16#bd# => -- LDA Absolute, X
            lda(addr_abs_indexed(ACT_X)),
        16#be# => -- LDX Absolute, Y
            ldx(addr_abs_indexed(ACT_Y)),
        16#bf# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#c0# => -- CPY Immediate
            cmp(ACT_Y, addr_imm),
        16#c1# => -- CMP (Indirect, X)
            cmp(ACT_A, addr_indexed_indirect),
        16#c2# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#c3# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#c4# => -- CPY Zero Page
            cmp(ACT_Y, addr_zp),
        16#c5# => -- CMP Zero Page
            cmp(ACT_A, addr_zp),
        16#c6# => -- DEC Zero Page
            dec(rmw(addr_zp)),
        16#c7# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#c8# => -- INY
            iny,
        16#c9# => -- CMP Immediate
            cmp(ACT_A, addr_imm),
        16#ca# => -- DEX
            dex,
        16#cb# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#cc# => -- CPY Absolute
            cmp(ACT_Y, addr_abs),
        16#cd# => -- CMP Absolute
            cmp(ACT_A, addr_abs),
        16#ce# => -- DEC Absolute
            dec(rmw(addr_abs)),
        16#cf# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#d0# => -- BNE
            branch,
        16#d1# => -- CMP (Indirect), Y
            cmp(ACT_A, addr_indirect_indexed),
        16#d2# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#d3# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#d4# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#d5# => -- CMP Zero Page, X
            cmp(ACT_A, addr_zp_indexed(ACT_X)),
        16#d6# => -- DEC Zero Page, X
            dec(rmw(addr_zp_indexed(ACT_X))),
        16#d7# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#d8# => -- CLD
            act_p_d(ACT_CLR),
        16#d9# => -- CMP Absolute Y
            cmp(ACT_A, addr_abs_indexed(ACT_Y)),
        16#da# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#db# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#dc# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#dd# => -- CMP Absolute, X
            cmp(ACT_A, addr_abs_indexed(ACT_X)),
        16#de# => -- DEC Absolute, X
            dec(rmw(addr_abs_indexed(ACT_X, always_page_cross => true))),
        16#df# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#e0# => -- CPX Immediate
            cmp(ACT_X, addr_imm),
        16#e1# => -- SBC (Indirect, X)
            arith(ACT_SBC, addr_indexed_indirect),
        16#e2# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#e3# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#e4# => -- CPX Zero Page
            cmp(ACT_X, addr_zp),
        16#e5# => -- SBC Zero Page
            arith(ACT_SBC, addr_zp),
        16#e6# => -- INC Zero Page
            inc(rmw(addr_zp)),
        16#e7# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#e8# => -- INX
            inx,
        16#e9# => -- SBC Immediate
            arith(ACT_SBC, addr_imm),
        16#ea# => -- NOP
            num_cycles(2),
        16#eb# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#ec# => -- CPX Absolute
            cmp(ACT_X, addr_abs),
        16#ed# => -- SBC Absolute
            arith(ACT_SBC, addr_abs),
        16#ee# => -- INC Absolute
            inc(rmw(addr_abs)),
        16#ef# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#f0# => -- BEQ
            branch,
        16#f1# => -- SBC (Indirect), Y
            arith(ACT_SBC, addr_indirect_indexed),
        16#f2# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#f3# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#f4# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#f5# => -- SBC Zero Page, X
            arith(ACT_SBC, addr_zp_indexed(ACT_X)),
        16#f6# => -- INC Zero Page, X
            inc(rmw(addr_zp_indexed(ACT_X))),
        16#f7# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#f8# => -- SED
            act_p_d(ACT_SET),
        16#f9# => -- SBC Absolute, Y
            arith(ACT_SBC, addr_abs_indexed(ACT_Y)),
        16#fa# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#fb# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#fc# => -- Future expansion
            (others => DEFAULT_STEP_ACT),
        16#fd# => -- SBC Absolute, X
            arith(ACT_SBC, addr_abs_indexed(ACT_X)),
        16#fe# => -- INC Absolute, X
            inc(rmw(addr_abs_indexed(ACT_X, always_page_cross => true))),
        16#ff# => -- Future expansion
            (others => DEFAULT_STEP_ACT)
    );

    -- instruction step counter
    signal current_step : natural range 0 to 7;
    -- actions for current cycle
    signal current_act : step_act;

    type interrupt is (NONE, RESET);

    -- constants for some instructions
    constant BRK : std_logic_vector(7 downto 0) := x"00";

    signal pending : interrupt;

    -- predecode signals and hold ir signal
    signal pd : std_logic_vector(7 downto 0);
    signal pdi : std_logic_vector(7 downto 0);
    signal hold_ir : std_logic_vector(7 downto 0);

    signal branch_cond : std_logic;

    -- status flags
    signal p_c : std_logic;
    signal p_z : std_logic;
    signal p_i : std_logic;
    signal p_d : std_logic;
    signal p_v : std_logic;
    signal p_n : std_logic;

    -- registers
    signal temp : std_logic_vector(7 downto 0);
    signal adl : std_logic_vector(7 downto 0);
    signal adh : std_logic_vector(7 downto 0);
    signal page_cross : std_logic;

    -- alu signals
    signal alu_a : std_logic_vector(7 downto 0);
    signal alu_b : std_logic_vector(7 downto 0);
    signal alu_cin : std_logic;
    signal alu_op : std_logic_vector(2 downto 0);
    signal alu_x : std_logic_vector(7 downto 0);
    signal alu_cout : std_logic;
    signal alu_z : std_logic;
    signal alu_v : std_logic;
begin

    -- Any following behavior is directed by the current_act bit field
    current_act <= fetch when current_step = 0 else
                    decode_rom(to_integer(unsigned(ir)))(current_step);

    -- current_step
    process(clk, areset)
        variable next_step : natural range 0 to 7;
        variable skip : natural range 0 to 7;
    begin
        if areset = '1' then
            current_step <= 0;
        elsif rising_edge(clk) and run = '1' then
            next_step := current_step + 1;
            skip := current_step + 2;

            case current_act.step_count is
                when ACT_INC =>
                    current_step <= next_step;
                when ACT_RESET =>
                    current_step <= 0;
                when ACT_RESET_NO_BRANCH =>
                    if branch_cond = '0' then
                        current_step <= 0;
                    else
                        current_step <= next_step;
                    end if;
                when ACT_SKIP_NO_C =>
                    if alu_cout = '0' then
                        current_step <= skip;
                    else
                        current_step <= next_step;
                    end if;
            end case;
        end if;
    end process;

    -- Calculate the branch condition when ir is a branch instruction
    -- 2 MSB indicate which status flags is tests, 3rd MSB inverts
    -- the condition
    process(all)
        variable cond : std_logic;
    begin
        case ir(7 downto 6) is
            when "00" => cond := p_n;
            when "01" => cond := p_v;
            when "10" => cond := p_c;
            when "11" => cond := p_z;
            when others => cond := '-';
        end case;

        if ir(5) = '0' then
            cond := not cond;
        end if;

        branch_cond <= cond;
    end process;

    -- sync decode
    sync <= '1' when current_step = 1 else '0';

    -- TODO: this is a remnant where step was a one hot step counter
    -- it is still used by the softcore to decide if the current step is
    -- the first step (fetch) of an instruction.
    process(all)
        constant sync_step : std_logic_vector(7 downto 0) := x"80";
    begin
        step <= std_logic_vector(shift_right(unsigned(sync_step), current_step));
    end process;

    -- pending interrupt logic
    process(areset, clk)
    begin
        if areset = '1' then
            pending <= RESET;
        elsif rising_edge(clk) and run = '1' then
            -- pending interrupt is picked up in decode cycle
            -- clear it here
            if pending = RESET and current_step = 1 then
                pending <= NONE;
            end if;
        end if;
    end process;

    -- address logic
    process(all)
    begin
        -- we is reset in case dout is ACT_NONE
        we <= '1';
        case current_act.dout is
            when ACT_NONE =>
                we <= '0';
                dout <= "--------";
            when ACT_A =>
                dout <= a;
            when ACT_X =>
                dout <= x;
            when ACT_Y =>
                dout <= y;
            when ACT_PCL =>
                dout <= pc(7 downto 0);
            when ACT_PCH =>
                dout <= pc(15 downto 8);
            when ACT_P =>
                -- TODO no support for interrupts yet, so hardcode B-flag to 1
                dout <= p or "00010000";
            when ACT_ALU =>
                dout <= alu_x;
            when ACT_TEMP =>
                dout <= temp;
        end case;

        case current_act.addr is
            when ACT_PC =>
                addr <= pc;
            when ACT_S =>
                addr <= x"01" & s;
            when ACT_FFFC =>
                addr <= x"fffc";
            when ACT_FFFD =>
                addr <= x"fffd";
            when ACT_ZP_TEMP =>
                addr <= x"00" & temp;
            when ACT_AD =>
                addr <= adh & adl;
            when ACT_DC =>
                addr <= "----------------";
        end case;
    end process;

    -- pc register
    process(areset, clk)
    begin
        if areset = '1' then
            pc <= x"0000";
        elsif rising_edge(clk) and run = '1' then
            case current_act.pc is
                when ACT_HOLD =>
                when ACT_INC =>
                    pc <= std_logic_vector(unsigned(pc) + 1);
                when ACT_L_DIN =>
                    pc(7 downto 0) <= din;
                when ACT_H_DIN =>
                    pc(15 downto 8) <= din;
                when ACT_DIN_TEMP =>
                    pc <= din & temp;
                when ACT_L_ALU =>
                    pc(7 downto 0) <= alu_x;
            end case;
        end if;
    end process;

    -- predecode register
    process(areset, clk)
    begin
        if areset = '1' then
            pd <= x"00";
        elsif rising_edge(clk) and run = '1' then
            pd <= din;
        end if;
    end process;

    -- predecode logic
    pdi <= BRK when pending = RESET else
           pd;

    -- instruction register
    -- make ir a transparant latch. ir gets a
    -- a new value in the second cycle of the instruction (first cycle
    -- being the fetch). the instruction is then stored in the ir.
    -- To be able to start decoding the instruction in the second cycle,
    -- the output of the predecode is forwarded during the second cycle.
    ir <= pdi when current_step = 1 else
          hold_ir;
    process(areset, clk)
    begin
        if areset = '1' then
            hold_ir <= x"00";
        elsif rising_edge(clk) and run = '1' and current_step = 1 then
            hold_ir <= pdi;
        end if;
    end process;

    -- status flags
    p <= p_n & p_v & "10" & p_d & p_i & p_z & p_c;

    process(areset, clk)
    begin
        if areset = '1' then
            p_c <= '0';
            p_z <= '0';
            p_i <= '0';
            p_d <= '0';
            p_v <= '0';
            p_n <= '0';
        elsif rising_edge(clk) and run = '1' then
            case current_act.p_i is
                when ACT_HOLD =>
                when ACT_SET =>
                    p_i <= '1';
                when ACT_CLR =>
                    p_i <= '0';
                when ACT_DIN2 =>
                    p_i <= din(2);
            end case;

            case current_act.p_c is
                when ACT_HOLD =>
                when ACT_SET =>
                    p_c <= '1';
                when ACT_CLR =>
                    p_c <= '0';
                when ACT_ALU =>
                    p_c <= alu_cout;
                when ACT_DIN0 =>
                    p_c <= din(0);
            end case;

            case current_act.p_z is
                when ACT_HOLD =>
                when ACT_ALU =>
                    p_z <= alu_z;
                when ACT_DIN1 =>
                    p_z <= din(1);
            end case;

            case current_act.p_n is
                when ACT_HOLD =>
                when ACT_ALU =>
                    p_n <= alu_x(7);
                when ACT_DIN7 =>
                    p_n <= din(7);
            end case;

            case current_act.p_v is
                when ACT_HOLD =>
                when ACT_ALU =>
                    p_v <= alu_v;
                when ACT_CLR =>
                    p_v <= '0';
                when ACT_DIN6 =>
                    p_v <= din(6);
            end case;

            case current_act.p_d is
                when ACT_HOLD =>
                when ACT_SET =>
                    p_d <= '1';
                when ACT_CLR =>
                    p_d <= '0';
                when ACT_DIN3 =>
                    p_d <= din(3);
            end case;
        end if;

    end process;

    -- stack pointer
    process(areset, clk)
    begin
        if areset = '1' then
            s <= x"00";
        elsif rising_edge(clk) and run = '1' then
            case current_act.s is
                when ACT_HOLD =>
                when ACT_DEC =>
                    s <= std_logic_vector(unsigned(s) - 1);
                when ACT_INC =>
                    s <= std_logic_vector(unsigned(s) + 1);
                when ACT_ALU =>
                    s <= alu_x;
            end case;

        end if;
    end process;

    -- A register
    process(areset, clk)
    begin
        if areset = '1' then
            a <= x"00";
        elsif rising_edge(clk) and run = '1' then
            case current_act.a is
                when ACT_HOLD =>
                when ACT_ALU =>
                    a <= alu_x;
            end case;
        end if;
    end process;

    -- X register
    process(areset, clk)
    begin
        if areset = '1' then
            x <= x"00";
        elsif rising_edge(clk) and run = '1' then
            case current_act.x is
                when ACT_HOLD =>
                when ACT_ALU =>
                    x <= alu_x;
            end case;
        end if;
    end process;

    -- Y register
    process(areset, clk)
    begin
        if areset = '1' then
            y <= x"00";
        elsif rising_edge(clk) and run = '1' then
            case current_act.y is
                when ACT_HOLD =>
                when ACT_ALU =>
                    y <= alu_x;
            end case;
        end if;
    end process;

    -- temp register
    process(areset, clk)
    begin
        if areset = '1' then
            temp <= x"00";
        elsif rising_edge(clk) and run = '1' then
            case current_act.temp is
                when ACT_DC =>
                    temp <= "--------";
                when ACT_DIN =>
                    temp <= din;
                when ACT_HOLD =>
                when ACT_ALU =>
                    temp <= alu_x;
                when ACT_ALU_COUT =>
                    temp <= "0000000" & alu_cout;

            end case;
        end if;
    end process;

    -- adl
    process(areset, clk)
    begin
        if areset = '1' then
            adl <= x"00";
        elsif rising_edge(clk) and run = '1' then
            case current_act.adl is
                when ACT_DC =>
                    adl <= "--------";
                when ACT_HOLD =>
                when ACT_DIN =>
                    adl <= din;
                when ACT_ALU =>
                    adl <= alu_x;
            end case;
        end if;
    end process;

    -- adh
    process(areset, clk)
    begin
        if areset = '1' then
            adh <= x"00";
        elsif rising_edge(clk) and run = '1' then
            case current_act.adh is
                when ACT_DC =>
                    adh <= "--------";
                when ACT_HOLD =>
                when ACT_DIN =>
                    adh <= din;
                when ACT_00 =>
                    adh <= x"00";
                when ACT_ALU =>
                    adh <= alu_x;
            end case;
        end if;
    end process;

    -- page_cross
    process(areset, clk)
    begin
        if areset = '1' then
            page_cross <= '0';
        elsif rising_edge(clk) and run = '1' then
            case current_act.page_cross is
                when ACT_DC =>
                    page_cross <= '-';
                when ACT_HOLD =>
                when ACT_ALU_C =>
                    page_cross <= alu_cout;
            end case;
        end if;
    end process;

    -- alu (name alu already in use)
    al_unit : entity work.nes_alu
        port map(
            a => alu_a,
            b => alu_b,
            cin => alu_cin,
            op => alu_op,
            x => alu_x,
            cout => alu_cout,
            z => alu_z,
            v => alu_v);

    -- alu input signals
    process(all)
    begin
        case current_act.alu_op is
            when ACT_DC =>
                alu_op <= "---";
            when ACT_ADC =>
                alu_op <= "011";
            when ACT_AND =>
                alu_op <= "001";
            when ACT_CMP =>
                alu_op <= "110";
            when ACT_SBC =>
                alu_op <= "111";
            when ACT_OR =>
                alu_op <= "000";
            when ACT_XOR =>
                alu_op <= "010";
            when ACT_LSR =>
                alu_op <= "100";
        end case;

        case current_act.alu_a is
            when ACT_DC =>
                alu_a <= "--------";
            when ACT_DIN =>
                alu_a <= din;
            when ACT_PCL =>
                alu_a <= pc(7 downto 0);
            when ACT_A =>
                alu_a <= a;
            when ACT_S =>
                alu_a <= s;
            when ACT_X =>
                alu_a <= x;
            when ACT_Y =>
                alu_a <= y;
            when ACT_TEMP =>
                alu_a <= temp;
            when ACT_ADL =>
                alu_a <= adl;
            when ACT_ADH =>
                alu_a <= adh;
        end case;

        case current_act.alu_b is
            when ACT_DC =>
                alu_b <= "--------";
            when ACT_A =>
                alu_b <= a;
            when ACT_X =>
                alu_b <= x;
            when ACT_Y =>
                alu_b <= y;
            when ACT_00 =>
                alu_b <= x"00";
            when ACT_TEMP =>
                alu_b <= temp;
            when ACT_DIN =>
                alu_b <= din;
        end case;

        case current_act.alu_cin is
            when ACT_DC =>
                alu_cin <= '-';
            when ACT_0 =>
                alu_cin <= '0';
            when ACT_1 =>
                alu_cin <= '1';
            when ACT_P_C =>
                alu_cin <= p_c;
            when ACT_PX =>
                alu_cin <= page_cross;
        end case;
    end process;
end structural;
