library ieee;
use ieee.std_logic_1164.all;

use ieee.numeric_std.all;

entity nes_video is
    port (
        nes_clk : in std_logic;
        nes_reset : in std_logic;
        vga_clk : in std_logic;

        -- vga interface
        vga_r : out std_logic_vector(3 downto 0);
        vga_g : out std_logic_vector(3 downto 0);
        vga_b : out std_logic_vector(3 downto 0);
        vga_hs : out std_logic;
        vga_vs : out std_logic
    );
end nes_video;

architecture structural of nes_video is
    signal vga_areset_sync : std_logic_vector(0 to 1);
    signal vga_fifo_full_sync : std_logic_vector(0 to 1);
    signal vga_areset : std_logic;
    signal vga_pixel_pop : std_logic;

    signal fifo_wrdy : std_logic;
    signal fifo_wen : std_logic;
    signal fifo_wdata : std_logic_vector(11 downto 0);
    signal fifo_full : std_logic;

    signal fifo_rrdy : std_logic;
    signal fifo_ren : std_logic;
    signal fifo_rdata : std_logic_vector(11 downto 0);
    signal fifo_empty : std_logic;

    signal conv_i : std_logic_vector(5 downto 0);
    signal conv_ivalid : std_logic;
    signal conv_irdy : std_logic;

    signal conv_o : std_logic_vector(11 downto 0);
    signal conv_ovalid : std_logic;
    signal conv_ordy : std_logic;

    signal pxcount : integer range 0 to 279;
begin
    vga : entity work.nes_vga
        port map(
            clk => vga_clk,
            areset => vga_areset,
            pop => vga_pixel_pop,
            rgb => fifo_rdata,
            vga_r => vga_r,
            vga_g => vga_g,
            vga_b => vga_b,
            vga_hs => vga_hs,
            vga_vs => vga_vs
        );

    pixel_fifo : entity work.fifo
        generic map(
            width => 12,
            depth_shift => 5) -- depth could be less?
        port map(
            areset => nes_reset,

            wclk => nes_clk,
            wrdy => fifo_wrdy,
            wen => fifo_wen,
            wdata => fifo_wdata,
            full => fifo_full,

            rclk => vga_clk,
            rrdy => fifo_rrdy,
            ren => fifo_ren,
            rdata => fifo_rdata,
            empty => fifo_empty);

    converter : entity work.video_converter
        port map(
            areset => nes_reset,
            clk => nes_clk,

            i => conv_i,
            ivalid => conv_ivalid,
            irdy => conv_irdy,

            o => conv_o,
            ovalid => conv_ovalid,
            ordy => conv_ordy
        );

    -- connect converter output to fifo
    fifo_wdata <= conv_o;
    conv_ordy <= fifo_wrdy and not fifo_full;
    fifo_wen <= conv_ovalid and conv_ordy;

    conv_ivalid <= '1';

    process(nes_reset, nes_clk)
    begin
        if nes_reset = '1' then
            conv_i <= "000000";
            pxcount <= 0;
        elsif rising_edge(nes_clk) then
            if conv_irdy = '1' then
                if pxcount = 279 then
                    pxcount <= 0;
                    conv_i <= "000000";
                else
                    pxcount <= pxcount + 1;
                    if pxcount mod 4 = 3 then
                        conv_i <= std_logic_vector(unsigned(conv_i) + 1);
                    end if;
                end if;
            end if;
        end if;
    end process;

    -- connect fifo to vga output
    fifo_ren <= vga_pixel_pop and not fifo_empty and fifo_rrdy;

    -- at startup the vga reads from the fifo before data is available. So keep
    -- vga areset asserted until fifo is full to avoid start up glitches.
    vga_areset <= vga_areset_sync(0) or not vga_fifo_full_sync(0) or not fifo_rrdy;
    process(nes_reset, vga_clk)
    begin
        if nes_reset = '1' then
            vga_areset_sync <= (others => '1');
            vga_fifo_full_sync <= (others => '0');
        elsif rising_edge(vga_clk) then
            vga_areset_sync <= vga_areset_sync(1) & '0';
            vga_fifo_full_sync <= vga_fifo_full_sync(1) & (fifo_full and fifo_wrdy);
        end if;
    end process;
end structural;

