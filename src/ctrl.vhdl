library ieee;
use ieee.std_logic_1164.all;


entity nes_ctrl is
    port (
        --! asynchronous reset
        reset : in std_logic;
        --! clock signal
        clk : in std_logic;

        -- input ctrl signals

        --! a rising edge of the step signals triggers the cpu to run for a
        --! single clock cycle. Only signal a rising edge if the cpu is idle
        --! (not busy).
        step : in std_logic;

        --! Keep the cpu running
        run : in std_logic;

        -- output status signals
        --! signals if the cpu is busy
        busy : out std_logic;

        -- input status signals

        --! rdy signal from the cpu bus
        cpu_bus_rdy : in std_logic;

        -- output ctrl signals
        --! cpu run strobe
        cpu_run : out std_logic;
        --! bus enable signal
        mem_run : out std_logic
    );
end nes_ctrl;

architecture impl of nes_ctrl is
    signal step_prev : std_logic;
    signal step_pulse : std_logic;
begin

    process(reset, clk)
    begin
        if reset = '1' then
            step_prev <= '0'; 
        elsif rising_edge(clk) then
            step_prev <= step;
        end if;
    end process;

    -- create a single cycle pulse on a rising edge of step
    step_pulse <= not step_prev and step;

    -- cpu can only run if the bus is idle
    cpu_run <= (step_pulse or run) and cpu_bus_rdy;

    -- busy means either cpu is running or the busy is not ready
    busy <= (step_pulse or run) or not cpu_bus_rdy;

    -- Each cpu cycle results in a bus operation, hence
    -- mem_run is basically a single cycle delayed copy
    -- of cpu_run.
    process(reset, clk)
    begin
        if reset = '1' then
            mem_run <= '0';
        elsif rising_edge(clk) then
            mem_run <= cpu_run;
        end if;
    end process;
end impl;

