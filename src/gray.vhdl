library ieee;
use ieee.std_logic_1164.all;

-- Translate a std_logic_vector to a gray encoding
entity gray_encoder is
    generic(
        width : positive
    );
    port(
        i : in std_logic_vector(width - 1 downto 0);
        o : out std_logic_vector(width - 1 downto 0)
    );
end gray_encoder;

architecture behavioral of gray_encoder is
begin
    o(width - 1) <= i(width - 1);

    gen_gray : for k in width-2 downto 0 generate
        o(k) <= i(k) xor i(k+1);
    end generate gen_gray;
end behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity gray_decoder is
    generic(
        width : positive
    );
    port(
        i : in std_logic_vector(width-1 downto 0);
        o : out std_logic_vector(width-1 downto 0)
    );
end gray_decoder;


architecture behavioral of gray_decoder is
    signal t : std_logic_vector(width-1 downto 0);
begin
    o <= t;

    t(width - 1) <= i(width - 1);

    gen_gray : for k in width-2 downto 0 generate
        t(k) <= i(k) xor t(k+1);
    end generate gen_gray;
end behavioral;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.all;

entity gray_sync is
    generic(
        width : positive);
    port(
        areset : in std_logic;
        iclk : in std_logic;
        i : out std_logic_vector(width-1 downto 0);
        inc : in std_logic;

        oclk : in std_logic;
        o : out std_logic_vector(width-1 downto 0)
    );
end gray_sync;

architecture structural of gray_sync is

    signal next_i : std_logic_vector(width-1 downto 0);
    signal gray_next_i : std_logic_vector(width-1 downto 0);
    signal gray_i : std_logic_vector(width-1 downto 0);

    type synchronizer is array(0 to 1) of std_logic_vector(width-1 downto 0);
    signal sync : synchronizer;
begin

    encoder : entity gray_encoder
        generic map(
            width => width)
        port map(
            i => next_i,
            o => gray_next_i
        );

    decoder : entity gray_decoder
        generic map(
            width => width)
        port map(
            i => sync(1),
            o => o
        );

    -- increment process
    process(areset, iclk)
    begin
        if areset = '1' then
            i <= (others => '0');
            gray_i <= (others => '0');
        elsif rising_edge(iclk) then
            if inc = '1' then
                i <= next_i;
                gray_i <= gray_next_i;
            end if;
        end if;
    end process;

    next_i <= std_logic_vector(unsigned(i) + 1);

    -- synchronizer to output clock domain
    process(areset, oclk)
    begin
        if areset = '1' then
            sync <= (others => (others => '0'));
        elsif rising_edge(oclk) then
            sync <= (gray_i, sync(0));
        end if;
    end process;

end structural;
