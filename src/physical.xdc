# 100MHz sys clock
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN E3 } [get_ports ext_clk]

# reset button
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN C12 } [get_ports resetn]

# vga pins
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN A3 } [get_ports vga_r[0]]
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN B4 } [get_ports vga_r[1]]
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN C5 } [get_ports vga_r[2]]
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN A4 } [get_ports vga_r[3]]
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN C6 } [get_ports vga_g[0]]
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN A5 } [get_ports vga_g[1]]
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN B6 } [get_ports vga_g[2]]
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN A6 } [get_ports vga_g[3]]
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN B7 } [get_ports vga_b[0]]
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN C7 } [get_ports vga_b[1]]
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN D7 } [get_ports vga_b[2]]
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN D8 } [get_ports vga_b[3]]
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN B11 } [get_ports vga_hs]
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN B12 } [get_ports vga_vs]

# uart pins
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN D4 } [get_ports uart_txd]
set_property -dict { IOSTANDARD LVCMOS33 PACKAGE_PIN C4 } [get_ports uart_rxd]

# place vga output flipflops in IOB
set_property IOB TRUE [get_ports vga_*]
