
library ieee;
use ieee.std_logic_1164.all;
library unisim;
use unisim.vcomponents.all;


entity clk is
    port (
        clk100mhz : in std_logic;
        reset : in std_logic;
        locked : out std_logic;
        vga_clk : out std_logic;
        nes_clk : out std_logic
    );
end clk;

architecture behavioral of clk is
    signal clkfb : std_logic;
    signal clkout0 : std_logic;
    signal clkout1 : std_logic;
begin
    mmcm: unisim.vcomponents.mmcme2_base
        generic map(
            divclk_divide => 4,
            clkfbout_mult_f => 48.0,
            -- This gives 25.197Mhz, close enough for vga (25.175Mhz)
            clkout0_divide_f => 47.625,
            clkout1_divide => 12
        )
        port map(
            clkin1 => clk100mhz,
            clkfbin => clkfb,
            clkfbout => clkfb,
            clkout0 => clkout0,
            clkout1 => clkout1,
            pwrdwn => '0',
            rst => reset,
            locked => locked
        );

    vga_clk_buf: unisim.vcomponents.bufg
        port map(
            i => clkout0,
            o => vga_clk
        );
    nes_clk_buf: unisim.vcomponents.bufg
        port map(
            i => clkout1,
            o => nes_clk
        );
end behavioral;
