library ieee;
use ieee.std_logic_1164.all;

use ieee.numeric_std.all;

-- output vga video 640x480@60Hz
entity nes_vga is
    port (
        clk : in std_logic; -- the pixel clock 25.175Mhz
        areset : in std_logic;

        -- pixel data input
        pop : out std_logic;
        rgb : in std_logic_vector(11 downto 0);
        
        -- vga interface
        vga_r : out std_logic_vector(3 downto 0);
        vga_g : out std_logic_vector(3 downto 0);
        vga_b : out std_logic_vector(3 downto 0);
        vga_hs : out std_logic;
        vga_vs : out std_logic
    );
end nes_vga;

architecture behavioral of nes_vga is
    constant hactive : integer := 640;
    constant hfront : integer := 16;
    constant hsync : integer := 96;
    constant hback : integer := 48;
    constant htotal : integer := hactive + hfront + hsync + hback;
    constant hsync_start : integer := hactive + hfront;
    constant hsync_end : integer := hsync_start + hsync;

    constant vactive : integer := 480;
    constant vfront : integer := 10;
    constant vsync : integer := 2;
    constant vback : integer := 33;
    constant vtotal : integer := vactive + vfront + vsync + vback;
    constant vsync_start : integer := vactive + vfront;
    constant vsync_end : integer := vsync_start + vsync;

    signal line : unsigned(10 downto 0);
    signal col : unsigned(10 downto 0);
    signal active : std_logic;
    signal active_mask : std_logic_vector(3 downto 0);

    signal rgb_reg : std_logic_vector(11 downto 0);
    signal hs_reg : std_logic;
    signal vs_reg : std_logic;
begin
    -- pixel counters
    process(clk, areset)
    begin
        if areset = '1' then
            line <= to_unsigned(0, line'length);
            col <= to_unsigned(0, col'length);
        elsif rising_edge(clk) then
            if col = (htotal - 1) then
                col <= to_unsigned(0, col'length);

                if line = (vtotal - 1) then
                    line <= to_unsigned(0, line'length);
                else
                    line <= line + 1;
                end if;
            else 
                col <= col + 1;
            end if;
        end if;
    end process;

    -- hsync, negative polarity
    process(clk)
    begin
        if rising_edge(clk) then
            if col >= hsync_start and col < hsync_end then
                hs_reg <= '0';
            else
                hs_reg <= '1';
            end if;
        end if;
    end process;

    -- vsync, negative polarity
    process(clk)
    begin
        if rising_edge(clk) then
            if line >= vsync_start and line < vsync_end then
                vs_reg <= '0';
            else
                vs_reg <= '1';
            end if;
        end if;
    end process;

    -- active video
    process(line, col)
    begin
        if col < hactive and line < vactive then
            active <= '1';
        else
            active <= '0';
        end if;
    end process;

    active_mask <= active & active & active & active;
    pop <= active and not areset;

    process(clk)
    begin
        if rising_edge(clk) then
            rgb_reg <= rgb and active_mask & active_mask & active_mask;
        end if;
    end process;


    vga_r <= rgb_reg(11 downto 8);
    vga_g <= rgb_reg(7 downto 4);
    vga_b <= rgb_reg(3 downto 0);
    vga_hs <= hs_reg;
    vga_vs <= vs_reg;
end behavioral;
