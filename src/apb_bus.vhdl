library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Bridges 32-bit apb interface to the 8-bit cpu bus
-- 32-bit words are stored big endian
entity apb_cpu_bus is
    port (
        clk : in std_logic;
        reset : in std_logic;

        -- APB interface
        s_apb_psel : in std_logic;
        s_apb_penable : in std_logic;
        s_apb_pwrite : in std_logic;
        s_apb_pready : out std_logic;
        s_apb_pslverr : out std_logic;
        s_apb_paddr : in std_logic_vector(31 downto 0);
        s_apb_pwdata : in std_logic_vector(31 downto 0);
        s_apb_prdata : out std_logic_vector(31 downto 0);

        -- bus interface
        addr : out std_logic_vector(15 downto 0);
        en : out std_logic;
        we : out std_logic;
        rdy : in std_logic;
        din : in std_logic_vector(7 downto 0);
        dout : out std_logic_vector(7 downto 0)
    );
end apb_cpu_bus;

architecture impl of apb_cpu_bus is
    type state_type is (IDLE, WAIT_RDY, READ);

    signal state : state_type;

    signal byte_counter : natural range 0 to 3;
    signal wdata : std_logic_vector(31 downto 0);
begin

    s_apb_pslverr <= '0';

    -- s_apb_paddr, s_apb_pwrite are valid during the whole transaction
    addr <= std_logic_vector(unsigned(s_apb_paddr(15 downto 0)) + byte_counter);
    we <= s_apb_pwrite;

    -- during write wdata is shifted to output the correct byte.
    dout <= wdata(31 downto 24);
    
    process(reset, clk)
    begin
        if reset = '1' then
            s_apb_pready <= '0';
            s_apb_prdata <= x"00000000";
            en <= '0';
            byte_counter <= 0;
            wdata <= x"00000000";

            state <= IDLE;
        elsif rising_edge(clk) then
            case state is
                when IDLE =>
                    -- reset prready in case a previous transaction just finished
                    s_apb_pready <= '0';

                    if s_apb_psel = '1' then 
                        -- skip first READ state by asserting en already here
                        en <= '1';
                        byte_counter <= 0;
                        wdata <= s_apb_pwdata;
                        state <= WAIT_RDY;
                    end if;
                when WAIT_RDY =>
                    en <= '0';
                    if rdy = '1' then
                        -- shift data in/out
                        s_apb_prdata <= din & s_apb_prdata(31 downto 8);
                        wdata <= wdata(23 downto 0) & x"00";

                        if byte_counter = 3 then
                            s_apb_pready <= '1';
                            state <= IDLE;
                        else
                            state <= READ;
                        end if;

                        byte_counter <= byte_counter + 1;
                    end if;
                when READ =>
                    en <= '1';
                    state <= WAIT_RDY;
            end case;
        end if;
    end process;

end impl;
