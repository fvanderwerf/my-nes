-- video_hscale: scale the video from 280 to 640
-- using the nearest neighbor algorithm. Each 7 pixels are mapped to 16 pixels
-- The second and sixth pixel are output three times, the other pixels only two times.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity video_hscale is
    port(
        areset : in std_logic;
        clk : in std_logic;

        -- input interface
        i : in std_logic_vector(11 downto 0);
        ivalid : in std_logic;
        irdy : out std_logic;

        -- output interface
        o : out std_logic_vector(11 downto 0);
        ovalid : out std_logic;
        ordy : in std_logic
    );
end video_hscale;

architecture implementation of video_hscale is
    signal px_counter : unsigned(3 downto 0);
    signal next_input : std_logic;
begin
    o <= i;
    ovalid <= ivalid;
    -- pixel counts when the next input should be
    -- input pixel 1 and 5 are output three times, others only two.
    -- this follows the nearest neighbor mapping
    next_input <= '1' when px_counter = to_unsigned(1, px_counter'length) or
                           px_counter = to_unsigned(4, px_counter'length) or
                           px_counter = to_unsigned(6, px_counter'length) or
                           px_counter = to_unsigned(8, px_counter'length) or
                           px_counter = to_unsigned(10, px_counter'length) or
                           px_counter = to_unsigned(13, px_counter'length) or
                           px_counter = to_unsigned(15, px_counter'length) else
                  '0';
    irdy <= ordy and next_input;

    process(areset, clk)
    begin
        if areset = '1' then
            px_counter <= to_unsigned(0, px_counter'length);
        elsif rising_edge(clk) then
            if ordy = '1' and ivalid = '1' then
                px_counter <= px_counter + to_unsigned(1, px_counter'length);
            end if;
        end if;
    end process;

end implementation;

-- video vscale: scale the video vertically 2x
library ieee;
use ieee.std_logic_1164.all;
library unisim;
use unisim.vcomponents.all;

entity video_vscale is
    port(
        areset : in std_logic;
        clk : in std_logic;
        i : in std_logic_vector(11 downto 0);
        ivalid : in std_logic;
        irdy : out std_logic;
        o : out std_logic_vector(11 downto 0);
        ovalid : out std_logic;
        ordy : in std_logic
    );
end video_vscale;

architecture implementation of video_vscale is
    signal output : std_logic_vector(11 downto 0);
    signal output_valid : std_logic;
    signal input_ready : std_logic;

    signal output_handshake : std_logic;

    signal px_counter : natural range 0 to 279;
    signal oddline : std_logic;

    signal fifo_reset : std_logic_vector(0 to 5);
    signal fifo_di : std_logic_vector(31 downto 0);
    signal fifo_do : std_logic_vector(31 downto 0);
    signal fifo_rden : std_logic;
    signal fifo_wren : std_logic;

begin
    o <= output;
    ovalid <= output_valid;
    irdy <= input_ready;

    -- on even lines the input is routed directly the output
    -- on odd lines the fifo output is routed to the output
    output <= i when oddline = '0' else fifo_do(11 downto 0);
    output_valid <= ivalid and not fifo_reset(0) when oddline = '0' else '1';
    input_ready <= ordy and not fifo_reset(0) when oddline = '0' else '0';

    -- signals when a handshake occurs on the output
    output_handshake <= ordy and output_valid;

    -- pixel counter and odd line signal
    process(areset, clk)
    begin
        if areset = '1' then
            oddline <= '0';
            px_counter <= 0;
        elsif rising_edge(clk) then
            -- the pixel counter is used for both even and odd lines
            -- so increment when a handshake occurs on the output
            if output_handshake = '1' then
                if px_counter = 279 then
                    px_counter <= 0;
                    oddline <= not oddline;
                else
                    px_counter <= px_counter + 1;
                end if;
            end if;
        end if;
    end process;

    -- fifo reset must be asserted for five clks
    process(areset, clk)
    begin
        if areset = '1' then
            fifo_reset <= (others => '1');
        elsif rising_edge(clk) then
            fifo_reset <= fifo_reset(1 to 5) & '0';
        end if;
    end process;

    fifo_di <= (11 downto 0 => i, others => '0');
    -- store pixel line on even lines when a handshake occurs
    -- note, on even lines, output_handshake is same as the input handshake
    -- because input and output are connected directly for even lines.
    fifo_wren <= (not oddline) and output_handshake;
    -- read pixel line to output odd lines
    -- the fifo is configured for first word fall through so rden must
    -- be asserted when the handshake occurs
    fifo_rden <= oddline and output_handshake;
    fifo : unisim.vcomponents.fifo18e1
        generic map(
            first_word_fall_through => true,
            do_reg => 1,
            data_width => 18,
            fifo_mode => "FIFO18",
            en_syn => false)
        port map (
            rst => fifo_reset(0),
            rdclk => clk,
            wrclk => clk,
            di => fifo_di,
            dip => (others => '0'),
            do => fifo_do,
            rden => fifo_rden,
            wren => fifo_wren,
            regce => '0',
            rstreg => '0'
        );
end implementation;

-- do color conversion
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity color_convert is
    port(
        i : in std_logic_vector(5 downto 0);
        o : out std_logic_vector(11 downto 0)
    );
end color_convert;

architecture implementation of color_convert is
begin
    process(i)
    begin
        case i is
            when "000000" => o <= x"666";
            when "000001" => o <= x"037";
            when "000010" => o <= x"128";
            when "000011" => o <= x"418";
            when "000100" => o <= x"616";
            when "000101" => o <= x"713";
            when "000110" => o <= x"710";
            when "000111" => o <= x"620";
            when "001000" => o <= x"330";
            when "001001" => o <= x"130";
            when "001010" => o <= x"030";
            when "001011" => o <= x"042";
            when "001100" => o <= x"044";
            when "001101" => o <= x"000";
            when "001110" => o <= x"000";
            when "001111" => o <= x"000";

            when "010000" => o <= x"bbb";
            when "010001" => o <= x"16b";
            when "010010" => o <= x"45d";
            when "010011" => o <= x"84d";
            when "010100" => o <= x"a3b";
            when "010101" => o <= x"c37";
            when "010110" => o <= x"c43";
            when "010111" => o <= x"a50";
            when "011000" => o <= x"760";
            when "011001" => o <= x"370";
            when "011010" => o <= x"170";
            when "011011" => o <= x"084";
            when "011100" => o <= x"078";
            when "011101" => o <= x"000";
            when "011110" => o <= x"000";
            when "011111" => o <= x"000";

            when "100000" => o <= x"fff";
            when "100001" => o <= x"6bf";
            when "100010" => o <= x"9af";
            when "100011" => o <= x"d9f";
            when "100100" => o <= x"f8f";
            when "100101" => o <= x"f8c";
            when "100110" => o <= x"f98";
            when "100111" => o <= x"fa5";
            when "101000" => o <= x"cb3";
            when "101001" => o <= x"8c3";
            when "101010" => o <= x"5c5";
            when "101011" => o <= x"4d9";
            when "101100" => o <= x"4cd";
            when "101101" => o <= x"555";
            when "101110" => o <= x"000";
            when "101111" => o <= x"000";

            when "110000" => o <= x"fff";
            when "110001" => o <= x"cef";
            when "110010" => o <= x"def";
            when "110011" => o <= x"fdf";
            when "110100" => o <= x"fcf";
            when "110101" => o <= x"fde";
            when "110110" => o <= x"fdd";
            when "110111" => o <= x"fdb";
            when "111000" => o <= x"ee9";
            when "111001" => o <= x"deb";
            when "111010" => o <= x"cfc";
            when "111011" => o <= x"bfd";
            when "111100" => o <= x"bef";
            when "111101" => o <= x"bbb";
            when "111110" => o <= x"000";
            when "111111" => o <= x"000";

            when others => o <= x"0F0";
        end case;
    end process;
end implementation;

-- the video converter converts the ppu output to standard VGA 640x480
-- Note the ppu outputs 280x240 luma/chroma.
-- TODO: color conversion from luma/chroma to rgb
library ieee;
use ieee.std_logic_1164.all;

entity video_converter is
    port(
        areset : in std_logic;
        clk : in std_logic;

        -- input interface
        i : in std_logic_vector(5 downto 0);
        ivalid : in std_logic;
        irdy : out std_logic;

        -- output interface
        o : out std_logic_vector(11 downto 0);
        ovalid : out std_logic;
        ordy : in std_logic
    );
end video_converter;

architecture implementation of video_converter is
    signal i_rgb : std_logic_vector(11 downto 0);
    signal hscale_i: std_logic_vector(11 downto 0);
    signal hscale_ivalid: std_logic;
    signal hscale_irdy : std_logic;
begin
    color : entity work.color_convert
        port map(
            i => i,
            o => i_rgb);

    vscale : entity work.video_vscale
        port map(
            areset => areset,
            clk => clk,
            i => i_rgb,
            ivalid => ivalid,
            irdy => irdy,
            o => hscale_i,
            ovalid => hscale_ivalid,
            ordy => hscale_irdy
        );
    hscale : entity work.video_hscale
        port map(
            areset => areset,
            clk => clk,
            i => hscale_i,
            ivalid => hscale_ivalid,
            irdy => hscale_irdy,
            o => o,
            ovalid => ovalid,
            ordy => ordy
        );
end implementation;
