library ieee;
use ieee.std_logic_1164.all;

entity nes is
    port (
        nes_clk : in std_logic;
        nes_reset : in std_logic;
        vga_clk : in std_logic;

        -- vga interface
        vga_r : out std_logic_vector(3 downto 0);
        vga_g : out std_logic_vector(3 downto 0);
        vga_b : out std_logic_vector(3 downto 0);
        vga_hs : out std_logic;
        vga_vs : out std_logic;

        -- apb slave register interface
        s_apb_ri_psel : in std_logic;
        s_apb_ri_penable : in std_logic;
        s_apb_ri_pwrite : in std_logic;
        s_apb_ri_pready : out std_logic;
        s_apb_ri_pslverr : out std_logic;
        s_apb_ri_paddr : in std_logic_vector(31 downto 0);
        s_apb_ri_pwdata : in std_logic_vector(31 downto 0);
        s_apb_ri_prdata : out std_logic_vector(31 downto 0);

        -- apb slave memory interface
        s_apb_mi_psel : in std_logic;
        s_apb_mi_penable : in std_logic;
        s_apb_mi_pwrite : in std_logic;
        s_apb_mi_pready : out std_logic;
        s_apb_mi_pslverr : out std_logic;
        s_apb_mi_paddr : in std_logic_vector(31 downto 0);
        s_apb_mi_pwdata : in std_logic_vector(31 downto 0);
        s_apb_mi_prdata : out std_logic_vector(31 downto 0)
    );
end nes;


architecture structural of nes is
    attribute X_INTERFACE_INFO      : string;
    attribute X_INTERFACE_PARAMETER : string;

    attribute X_INTERFACE_INFO of nes_clk: signal is "xilinx.com:signal:clock:1.0 nes_clk CLK";
    attribute X_INTERFACE_PARAMETER of nes_clk: signal is "ASSOCIATED_RESET nes_reset";

    attribute X_INTERFACE_INFO of nes_reset: signal is "xilinx.com:signal:reset:1.0 nes_reset RST";
    attribute X_INTERFACE_PARAMETER of nes_reset: signal is "POLARITY ACTIVE_HIGH";

    attribute X_INTERFACE_INFO of vga_clk: signal is "xilinx.com:signal:clock:1.0 vga_clk CLK";

    -- apb register interface attributes
    attribute X_INTERFACE_INFO of s_apb_ri_psel: signal is "xilinx.com:interface:apb:1.0 RI_APB_S PSEL";
    attribute X_INTERFACE_INFO of s_apb_ri_penable: signal is "xilinx.com:interface:apb:1.0 RI_APB_S PENABLE";
    attribute X_INTERFACE_INFO of s_apb_ri_pwrite: signal is "xilinx.com:interface:apb:1.0 RI_APB_S PWRITE";
    attribute X_INTERFACE_INFO of s_apb_ri_pready: signal is "xilinx.com:interface:apb:1.0 RI_APB_S PREADY";
    attribute X_INTERFACE_INFO of s_apb_ri_pslverr: signal is "xilinx.com:interface:apb:1.0 RI_APB_S PSLVERR";
    attribute X_INTERFACE_INFO of s_apb_ri_paddr: signal is "xilinx.com:interface:apb:1.0 RI_APB_S PADDR";
    attribute X_INTERFACE_INFO of s_apb_ri_pwdata: signal is "xilinx.com:interface:apb:1.0 RI_APB_S PWDATA";
    attribute X_INTERFACE_INFO of s_apb_ri_prdata: signal is "xilinx.com:interface:apb:1.0 RI_APB_S PRDATA";

    -- apb memory interface attributes
    attribute X_INTERFACE_INFO of s_apb_mi_psel: signal is "xilinx.com:interface:apb:1.0 MI_APB_S PSEL";
    attribute X_INTERFACE_INFO of s_apb_mi_penable: signal is "xilinx.com:interface:apb:1.0 MI_APB_S PENABLE";
    attribute X_INTERFACE_INFO of s_apb_mi_pwrite: signal is "xilinx.com:interface:apb:1.0 MI_APB_S PWRITE";
    attribute X_INTERFACE_INFO of s_apb_mi_pready: signal is "xilinx.com:interface:apb:1.0 MI_APB_S PREADY";
    attribute X_INTERFACE_INFO of s_apb_mi_pslverr: signal is "xilinx.com:interface:apb:1.0 MI_APB_S PSLVERR";
    attribute X_INTERFACE_INFO of s_apb_mi_paddr: signal is "xilinx.com:interface:apb:1.0 MI_APB_S PADDR";
    attribute X_INTERFACE_INFO of s_apb_mi_pwdata: signal is "xilinx.com:interface:apb:1.0 MI_APB_S PWDATA";
    attribute X_INTERFACE_INFO of s_apb_mi_prdata: signal is "xilinx.com:interface:apb:1.0 MI_APB_S PRDATA";

    signal soft_reset : std_logic;
    signal step : std_logic;
    signal run : std_logic;
    signal busy : std_logic;

    signal cpu_run : std_logic;
    signal mem_run : std_logic;

    signal cpu_addr : std_logic_vector(15 downto 0);
    signal cpu_din : std_logic_vector(7 downto 0);
    signal cpu_dout : std_logic_vector(7 downto 0);
    signal cpu_en : std_logic;
    signal cpu_we : std_logic;
    signal cpu_rdy : std_logic;

    signal apb_addr : std_logic_vector(15 downto 0);
    signal apb_din : std_logic_vector(7 downto 0);
    signal apb_dout : std_logic_vector(7 downto 0);
    signal apb_en : std_logic;
    signal apb_we : std_logic;
    signal apb_rdy : std_logic;

    signal cpu_pc : std_logic_vector(15 downto 0);
    signal cpu_ir : std_logic_vector(7 downto 0);
    signal cpu_p : std_logic_vector(7 downto 0);
    signal cpu_a : std_logic_vector(7 downto 0);
    signal cpu_s : std_logic_vector(7 downto 0);
    signal cpu_x : std_logic_vector(7 downto 0);
    signal cpu_y : std_logic_vector(7 downto 0);
    signal cpu_step : std_logic_vector(7 downto 0);
begin

    regif : entity work.nes_regif
        port map(
            clk => nes_clk,
            reset => nes_reset,

            -- register interface
            s_apb_psel => s_apb_ri_psel,
            s_apb_penable => s_apb_ri_penable,
            s_apb_pwrite => s_apb_ri_pwrite,
            s_apb_pready => s_apb_ri_pready,
            s_apb_pslverr => s_apb_ri_pslverr,
            s_apb_paddr => s_apb_ri_paddr,
            s_apb_pwdata => s_apb_ri_pwdata,
            s_apb_prdata => s_apb_ri_prdata,

            -- nes cpu registers for debugging
            cpu_pc => cpu_pc,
            cpu_ir => cpu_ir,
            cpu_p => cpu_p,
            cpu_a => cpu_a,
            cpu_s => cpu_s,
            cpu_x => cpu_x,
            cpu_y => cpu_y,
            cpu_step => cpu_step,

            -- nes cpu mem signals for debugging
            cpu_addr => cpu_addr,
            cpu_din => cpu_din,
            cpu_dout => cpu_dout,
            cpu_we => cpu_we,
            cpu_rdy => cpu_rdy,

            -- ctrl signals
            soft_reset => soft_reset,
            step => step,
            run => run,
            -- status signals
            busy => busy
        );

    cpu_bus : entity work.cpu_bus
       port map(
            clk => nes_clk,

            -- cpu memory interface
            addr_a => cpu_addr,
            din_a => cpu_dout,
            dout_a => cpu_din,
            en_a => mem_run,
            we_a => cpu_we,
            rdy_a => cpu_rdy,

            addr_b => apb_addr,
            din_b => apb_dout,
            dout_b => apb_din,
            en_b => apb_en,
            we_b => apb_we,
            rdy_b => apb_rdy
       );

    apb_cpu_bus : entity work.apb_cpu_bus
        port map(
            reset => nes_reset,
            clk => nes_clk,

            -- register interface
            s_apb_psel => s_apb_mi_psel,
            s_apb_penable => s_apb_mi_penable,
            s_apb_pwrite => s_apb_mi_pwrite,
            s_apb_pready => s_apb_mi_pready,
            s_apb_pslverr => s_apb_mi_pslverr,
            s_apb_paddr => s_apb_mi_paddr,
            s_apb_pwdata => s_apb_mi_pwdata,
            s_apb_prdata => s_apb_mi_prdata,

            -- cpu memory interface
            addr => apb_addr,
            din => apb_din,
            dout => apb_dout,
            en => apb_en,
            we => apb_we,
            rdy => apb_rdy
       );

    nes : entity work.nes_cpu
        port map(
            clk => nes_clk,
            run => cpu_run,
            areset => soft_reset,
            addr => cpu_addr,
            din => cpu_din,
            dout => cpu_dout,
            we => cpu_we,
            pc => cpu_pc,
            ir => cpu_ir,
            p => cpu_p,
            a => cpu_a,
            s => cpu_s,
            x => cpu_x,
            y => cpu_y,
            step => cpu_step
        );

    ctrl : entity work.nes_ctrl
        port map(
            reset => soft_reset,
            clk => nes_clk,
            step => step,
            run => run,
            busy => busy,
            cpu_bus_rdy => cpu_rdy,
            cpu_run => cpu_run,
            mem_run => mem_run
        );

    video : entity work.nes_video
        port map(
            nes_clk => nes_clk,
            nes_reset => nes_reset,
            vga_clk => vga_clk,
            vga_r => vga_r,
            vga_g => vga_g,
            vga_b => vga_b,
            vga_hs => vga_hs,
            vga_vs => vga_vs
        );
end structural;
