library ieee;
use ieee.std_logic_1164.all;

-- Bridge between the NES cpu memory interface and
-- the AXI interconnect
--
-- Note, addr, dout, and we must remain valid throughout the
-- transaction, i.e., until ready becomes active again
--
-- This module is designed with a minimum number of delay cycles
-- Hence, signals to and from the AXI interface are forwarded directly
-- to the nes memory interface, and at the same time the signals
-- are latched in a register to maintain the signals in the following
-- cycles. Much like a traditional latch.
--
entity nes_aximem is
    generic (
        mem_base : std_logic_vector(31 downto 0) := x"8000_0000"
    );
    port (
        reset : in std_logic;
        clk : in std_logic;

        -- nes slave interface
        addr : in std_logic_vector(15 downto 0);
        din : out std_logic_vector(7 downto 0);
        dout : in std_logic_vector(7 downto 0);
        en : in std_logic;
        we : in std_logic;
        rdy : out std_logic;

        -- axi master interface
        m_axi_araddr : out std_logic_vector(31 downto 0);
        m_axi_arvalid : out std_logic;
        m_axi_arready : in std_logic;
        m_axi_arprot : out std_logic_vector(2 downto 0);

        m_axi_rvalid : in std_logic;
        m_axi_rready : out std_logic;
        m_axi_rdata : in std_logic_vector(31 downto 0);
        m_axi_rresp : in std_logic_vector(1 downto 0);

        m_axi_awaddr : out std_logic_vector(31 downto 0);
        m_axi_awvalid : out std_logic;
        m_axi_awready : in std_logic;
        m_axi_awprot : out std_logic_vector(2 downto 0);

        m_axi_wvalid : out std_logic;
        m_axi_wready : in std_logic;
        m_axi_wdata : out std_logic_vector(31 downto 0);
        m_axi_wstrb : out std_logic_vector(3 downto 0);

        m_axi_bvalid : in std_logic;
        m_axi_bready : out std_logic;
        m_axi_bresp : in std_logic_vector(1 downto 0)
    );
end nes_aximem;

architecture behavioral of nes_aximem is

    -- separate read and write enable signals
    signal ren : std_logic;
    signal wen : std_logic;

    signal ready_latch : std_logic;

    signal rdata : std_logic_vector(7 downto 0);
    signal rdata_latch : std_logic_vector(7 downto 0);

    type axi_out_state is (IDLE, VALID);

    signal ar_state : axi_out_state;
    signal aw_state : axi_out_state;
    signal w_state : axi_out_state;

begin
    ren <= en and not we;
    wen <= en and we;

    -- ready_latch is set when either read or write is done.
    -- rready and bready are always '1' therefore it is just a matter of
    -- waiting for a valid signal.
    process(reset, clk)
    begin
        if reset = '1' then
            ready_latch <= '1';
        elsif rising_edge(clk) then
            if en = '1' then
                -- starting new transaction
                ready_latch <= '0';
            elsif m_axi_rvalid = '1' or m_axi_bvalid = '1' then
                ready_latch <= '1';
            end if;
        end if;
    end process;

    rdy <= '0' when en = '1' else
           '1' when (m_axi_rvalid or m_axi_bvalid) else
            ready_latch;


    -- drive axi ar signals
    -- run ar_state statemachine
    process(reset, clk)
    begin
        if reset = '1' then
            ar_state <= IDLE;
        elsif rising_edge(clk) then
            if ren = '1' then
                -- arvalid is already high, so arvalid must be kept asserted
                -- longer only if arready is not yet high.
                if m_axi_arready = '0' then
                    ar_state <= VALID;
                end if;
            elsif ar_state = VALID then
                if m_axi_arready = '1' then
                    ar_state <= IDLE;
                end if;
            end if;
        end if;
    end process;

    m_axi_arvalid <= '1' when ren = '1' or ar_state = VALID else '0';

    -- forward read address to axi interface.
    -- ignore lower 2 bits to issue word aligned reads
    m_axi_araddr <= (15 downto 2 => addr(15 downto 2), 31 downto 16 => '0', 1 downto 0 => '0') or mem_base;
    m_axi_arprot <= (others => '0');
    m_axi_rready <= '1';

    -- correct for unaligned read
    process(m_axi_rdata, addr)
        variable strb : std_logic_vector(1 downto 0);
    begin
        case addr(1 downto 0) is
            when "00" =>
                rdata <= m_axi_rdata(7 downto 0);
            when "01" =>
                rdata <= m_axi_rdata(15 downto 8);
            when "10" =>
                rdata <= m_axi_rdata(23 downto 16);
            when others =>
                rdata <= m_axi_rdata(31 downto 24);
        end case;
    end process;

    -- latch incoming read data
    process(reset, clk)
    begin
        if reset = '1' then
            rdata_latch <= (others => '0');
        elsif rising_edge(clk) then
            if m_axi_rvalid = '1' then
                rdata_latch <= rdata;
            end if;
        end if;
    end process;

    -- forward received data
    din <= rdata when m_axi_rvalid = '1' else
           rdata_latch;


    -- drive aw signals

    -- run aw state machine
    process(reset, clk)
    begin
        if reset = '1' then
            aw_state <= IDLE;
        elsif rising_edge(clk) then
            if wen = '1' then
                if m_axi_awready = '0' then
                    aw_state <= VALID;
                end if;
            elsif aw_state = VALID then
                if m_axi_awready = '1' then
                    aw_state <= IDLE;
                end if;
            end if;
        end if;
    end process;

    m_axi_awvalid <= '1' when wen = '1' or aw_state = VALID else '0';

    -- forward write address to axi interface.
    -- ignore lower 2 bits to issue word aligned reads
    m_axi_awaddr <= (15 downto 2 => addr(15 downto 2), 31 downto 16 => '0', 1 downto 0 => '0') or mem_base;
    m_axi_awprot <= (others => '0');


    -- drive w signals
    -- run w state machine
    process(reset, clk)
    begin
        if reset = '1' then
            w_state <= IDLE;
        elsif rising_edge(clk) then
            if wen = '1' then
                if m_axi_wready = '0' then
                    w_state <= VALID;
                end if;
            elsif w_state = VALID then
                if m_axi_awready = '1' then
                    w_state <= IDLE;
                end if;
            end if;
        end if;
    end process;

    m_axi_wvalid <= '1' when wen = '0' or w_state = VALID else '0';

    -- put dout at correct position in wdata and set wstrb accordingly
    process(dout, addr)
        variable strb : std_logic_vector(1 downto 0);
    begin
        case addr(1 downto 0) is
            when "00" =>
                m_axi_wdata <= (7 downto 0 => dout, others => '0');
                m_axi_wstrb <= "0001";
            when "01" =>
                m_axi_wdata <= (15 downto 8 => dout, others => '0');
                m_axi_wstrb <= "0010";
            when "10" =>
                m_axi_wdata <= (23 downto 16 => dout, others => '0');
                m_axi_wstrb <= "0100";
            when others =>
                m_axi_wdata <= (31 downto 24 => dout, others => '0');
                m_axi_wstrb <= "1000";
        end case;
    end process;

    m_axi_bready <= '1';
end behavioral;
