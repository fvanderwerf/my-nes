library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cpu_bus is
    port (
        clk : in std_logic;

        addr_a : in std_logic_vector(15 downto 0);
        en_a : in std_logic;
        we_a : in std_logic;
        rdy_a : out std_logic;
        din_a : in std_logic_vector(7 downto 0);
        dout_a : out std_logic_vector(7 downto 0);

        addr_b : in std_logic_vector(15 downto 0);
        en_b : in std_logic;
        we_b : in std_logic;
        rdy_b : out std_logic;
        din_b : in std_logic_vector(7 downto 0);
        dout_b : out std_logic_vector(7 downto 0)
    );
end cpu_bus;

architecture structural of cpu_bus is
    type mem is array(0 to 16#ffff#) of std_logic_vector(7 downto 0);
    shared variable iram : mem;
begin

    -- port a
    process(clk)
    begin
        if rising_edge(clk) then
            if en_a = '1' then
                dout_a <= iram(to_integer(unsigned(addr_a)));
                if we_a = '1' then
                    iram(to_integer(unsigned(addr_a))) := din_a;
                end if;
            end if;
        end if;
    end process;

    -- port b
    process(clk)
    begin
        if rising_edge(clk) then
            if en_b = '1' then
                dout_b <= iram(to_integer(unsigned(addr_b)));
                if we_b = '1' then
                    iram(to_integer(unsigned(addr_b))) := din_b;
                end if;
            end if;
        end if;
    end process;

    -- rdy one cycle after en has been raised.
    rdy_a <= not en_a;
    rdy_b <= not en_b;
end structural;

