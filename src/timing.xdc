# timing exceptions for vga_areset and vga_fifo_full synchronization
## the fifo reset synchronizers
set_property KEEP TRUE [get_cells system_i/nes/inst/video/vga_areset_sync_reg[*]]
set_property KEEP TRUE [get_cells system_i/nes/inst/video/vga_fifo_full_sync_reg[*]]
set_property ASYNC_REG TRUE [get_cells system_i/nes/inst/video/vga_areset_sync_reg[*]]
set_property ASYNC_REG TRUE [get_cells system_i/nes/inst/video/vga_fifo_full_sync_reg[*]]
set_false_path -to [get_cells system_i/nes/inst/video/vga_areset_sync_reg[*]]
set_false_path -to [get_cells system_i/nes/inst/video/vga_fifo_full_sync_reg[*]]

# timing exceptions for the pixel fifo
## the fifo reset synchronizers
set_property KEEP TRUE [get_cells system_i/nes/inst/video/pixel_fifo/rreset_reg[*]]
set_property KEEP TRUE [get_cells system_i/nes/inst/video/pixel_fifo/wreset_reg[*]]
set_property ASYNC_REG TRUE [get_cells system_i/nes/inst/video/pixel_fifo/rreset_reg[*]]
set_property ASYNC_REG TRUE [get_cells system_i/nes/inst/video/pixel_fifo/wreset_reg[*]]
set_false_path -to [get_cells system_i/nes/inst/video/pixel_fifo/rreset_reg[*]]
set_false_path -to [get_cells system_i/nes/inst/video/pixel_fifo/wreset_reg[*]]

## the index synchronizers
set_property ASYNC_REG TRUE [get_cells system_i/nes/inst/video/pixel_fifo/windex_sync/sync_reg*]
set_property ASYNC_REG TRUE [get_cells system_i/nes/inst/video/pixel_fifo/rindex_sync/sync_reg*]
set_false_path -to [get_cells system_i/nes/inst/video/pixel_fifo/windex_sync/sync_reg[*][*]]
set_false_path -to [get_cells system_i/nes/inst/video/pixel_fifo/rindex_sync/sync_reg[*][*]]
set_bus_skew \
    -from [get_cells system_i/nes/inst/video/pixel_fifo/rindex_sync/gray_i_reg[*]] \
    -to [get_cells system_i/nes/inst/video/pixel_fifo/rindex_sync/sync_reg[1][*]] 2
set_bus_skew \
    -from [get_cells system_i/nes/inst/video/pixel_fifo/windex_sync/gray_i_reg[*]] \
    -to [get_cells system_i/nes/inst/video/pixel_fifo/windex_sync/sync_reg[1][*]] 2

## the fifo data
set_false_path -through [get_cells system_i/nes/inst/video/pixel_fifo/data_reg[*][*]]
