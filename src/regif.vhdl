library ieee;
use ieee.std_logic_1164.all;

entity nes_regif is
    port (
        clk : in std_logic;
        reset : in std_logic;

        s_apb_psel : in std_logic;
        s_apb_penable : in std_logic;
        s_apb_pwrite : in std_logic;
        s_apb_pready : out std_logic;
        s_apb_pslverr : out std_logic;
        s_apb_paddr : in std_logic_vector(31 downto 0);
        s_apb_pwdata : in std_logic_vector(31 downto 0);
        s_apb_prdata : out std_logic_vector(31 downto 0);

        cpu_pc : in std_logic_vector(15 downto 0);
        cpu_ir : in std_logic_vector(7 downto 0);
        cpu_p : in std_logic_vector(7 downto 0);
        cpu_a : in std_logic_vector(7 downto 0);
        cpu_s : in std_logic_vector(7 downto 0);
        cpu_x : in std_logic_vector(7 downto 0);
        cpu_y : in std_logic_vector(7 downto 0);
        cpu_step : in std_logic_vector(7 downto 0);

        cpu_addr : in std_logic_vector(15 downto 0);
        cpu_din : in std_logic_vector(7 downto 0);
        cpu_dout : in std_logic_vector(7 downto 0);
        cpu_we : in std_logic;
        cpu_rdy : in std_logic;

        soft_reset : out std_logic;
        step : out std_logic;
        run : out std_logic;
        busy : in std_logic
    );
end nes_regif;


architecture structural of nes_regif is
    signal paddr : std_logic_vector(11 downto 0);

    constant ADDR_CTRL : std_logic_vector(11 downto 0) := x"000";
    constant ADDR_STATUS : std_logic_vector(11 downto 0) := x"004";
    constant ADDR_NES_CPU_STEP : std_logic_vector(11 downto 0) := x"080";
    constant ADDR_NES_CPU_PC : std_logic_vector(11 downto 0) := x"084";
    constant ADDR_NES_CPU_IR : std_logic_vector(11 downto 0) := x"088";
    constant ADDR_NES_CPU_P : std_logic_vector(11 downto 0) := x"08c";
    constant ADDR_NES_CPU_A : std_logic_vector(11 downto 0) := x"090";
    constant ADDR_NES_CPU_S : std_logic_vector(11 downto 0) := x"094";
    constant ADDR_NES_CPU_X : std_logic_vector(11 downto 0) := x"098";
    constant ADDR_NES_CPU_Y : std_logic_vector(11 downto 0) := x"09c";

    constant ADDR_NES_CPU_MEM_ADDR : std_logic_vector(11 downto 0) := x"100";
    constant ADDR_NES_CPU_MEM_DIN : std_logic_vector(11 downto 0) := x"104";
    constant ADDR_NES_CPU_MEM_DOUT : std_logic_vector(11 downto 0) := x"108";
    constant ADDR_NES_CPU_MEM_WE : std_logic_vector(11 downto 0) := x"10c";
    constant ADDR_NES_CPU_MEM_RDY : std_logic_vector(11 downto 0) := x"110";

    constant ADDR_IDENT : std_logic_vector(11 downto 0) := x"F00";

    signal ctrl_reg : std_logic_vector(2 downto 0);
begin
    run <= ctrl_reg(2);
    step <= ctrl_reg(1);
    soft_reset <= ctrl_reg(0);

    -- register address space is 4KB, pick out the relevant bits
    paddr <= s_apb_paddr(11 downto 0);

    -- read register
    process(all)
    begin
        case paddr is
            when ADDR_CTRL =>
                s_apb_prdata <= (2 downto 0 => ctrl_reg, others => '0');
            when ADDR_STATUS =>
                s_apb_prdata <= (0 => busy, others => '0');
            when ADDR_IDENT =>
                s_apb_prdata <= x"4e455331"; -- "NES1"
            when ADDR_NES_CPU_STEP =>
                s_apb_prdata <= x"000000" & cpu_step;
            when ADDR_NES_CPU_PC =>
                s_apb_prdata <= x"0000" & cpu_pc;
            when ADDR_NES_CPU_IR =>
                s_apb_prdata <= x"000000" & cpu_ir;
            when ADDR_NES_CPU_P =>
                s_apb_prdata <= x"000000" & cpu_p;
            when ADDR_NES_CPU_A =>
                s_apb_prdata <= x"000000" & cpu_a;
            when ADDR_NES_CPU_S =>
                s_apb_prdata <= x"000000" & cpu_s;
            when ADDR_NES_CPU_X =>
                s_apb_prdata <= x"000000" & cpu_x;
            when ADDR_NES_CPU_Y =>
                s_apb_prdata <= x"000000" & cpu_y;
            when ADDR_NES_CPU_MEM_ADDR =>
                s_apb_prdata <= x"0000" & cpu_addr;
            when ADDR_NES_CPU_MEM_DIN =>
                s_apb_prdata <= x"000000" & cpu_din;
            when ADDR_NES_CPU_MEM_DOUT =>
                s_apb_prdata <= x"000000" & cpu_dout;
            when ADDR_NES_CPU_MEM_WE =>
                s_apb_prdata <= (0 => cpu_we, others => '0');
            when ADDR_NES_CPU_MEM_RDY =>
                s_apb_prdata <= (0 => cpu_rdy, others => '0');
            when others =>
                s_apb_prdata <= x"deadbeef";
        end case;
    end process;

    -- write register
    process(reset, clk)
    begin
        if reset = '1' then
            ctrl_reg <= "001";
        elsif rising_edge(clk) then
            if s_apb_psel = '1' and s_apb_penable = '1' and s_apb_pwrite = '1' then
                case paddr is
                    when ADDR_CTRL =>
                        ctrl_reg <= s_apb_pwdata(2 downto 0);
                    when others =>
                end case;
            end if;
        end if;
    end process;

    s_apb_pready <= '1';
    s_apb_pslverr <= '0';

end structural;
