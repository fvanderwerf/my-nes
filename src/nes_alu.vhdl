library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity nes_alu is
    port (
        a : in std_logic_vector(7 downto 0);
        b : in std_logic_vector(7 downto 0);
        cin : in std_logic;
        op : in std_logic_vector(2 downto 0);

        x : inout std_logic_vector(7 downto 0);
        cout : out std_logic;
        z : out std_logic;
        v : out std_logic
    );
end nes_alu;

architecture behavioral of nes_alu is
begin
    process(a, b, cin, op)
        variable inv_b : unsigned(7 downto 0);
        variable nb : unsigned(7 downto 0);
        variable result : std_logic_vector(7 downto 0);
        variable sum : unsigned(8 downto 0);
    begin

        inv_b := unsigned(not b);
        nb := inv_b + 1;

        cout <= '-';
        v <= '-';

        case op is
            when "000" =>
                result := a or b;
            when "001" =>
                result := a and b;
            when "010" =>
                result := a xor b;
            when "011" => -- ADC
                sum := unsigned("0" & a) + unsigned("0" & b) +
                        unsigned(std_logic_vector'("00000000") & cin);
                cout <= sum(8);
                result := std_logic_vector(sum(7 downto 0));
                v <= (a(7) xnor b(7)) and (result(7) xor a(7));
            when "100" => -- LSR/ROR
                result := cin & a(7 downto 1);
                cout <= a(0);
            when "110" => -- CMP
                sum := unsigned("0" & a) + (unsigned'("0") & nb);
                result := std_logic_vector(sum(7 downto 0));

                if unsigned(a) >= unsigned(b) then
                    cout <= '1';
                else
                    cout <= '0';
                end if;
            when "111" => -- SBC
                sum := unsigned("0" & a) + (unsigned'("0") & inv_b) +
                        unsigned(std_logic_vector'("00000000") & cin);
                result := std_logic_vector(sum(7 downto 0));
                v <= (a(7) xnor inv_b(7)) and (result(7) xor a(7));
                cout <= sum(8);
            when others =>
                result := "--------";
        end case;

        if result = x"00" then
            z <= '1';
        else
            z <= '0';
        end if;

        x <= result;
    end process;
end behavioral;
