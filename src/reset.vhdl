
library ieee;
use ieee.std_logic_1164.all;

-- synchronize the release of the reset in multiple clock domains
entity reset is
    port (
        sys_reset : in std_logic;
        locked : in std_logic;
        vga_clk : in std_logic;
        vga_areset : out std_logic;
        nes_clk : in std_logic;
        nes_areset : out std_logic
    );
end reset;

architecture behavioral of reset is
    signal areset : std_logic;

    signal vga_reset_sync : std_logic_vector(1 downto 0);
    signal nes_reset_sync : std_logic_vector(1 downto 0);
begin

    areset <= sys_reset or not locked;

    process(areset, vga_clk)
    begin
        if areset = '1' then
            vga_reset_sync <= "11";
        elsif rising_edge(vga_clk) then
            -- double flipflop the reset signal to cross clock domain
            vga_reset_sync <= vga_reset_sync(0) & '0';
        end if;
    end process;

    vga_areset <= vga_reset_sync(1);

    process(areset, nes_clk)
    begin
        if areset = '1' then
            nes_reset_sync <= "11";
        elsif rising_edge(nes_clk) then
            -- double flipflop the reset signal to cross clock domain
            nes_reset_sync <= nes_reset_sync(0) & '0';
        end if;
    end process;

    nes_areset <= nes_reset_sync(1);
end behavioral;
