#!/bin/sh -e
VIVADO_BIN=/home/fabian/tools/Xilinx/Vivado/2019.1/bin
XSIM=${VIVADO_BIN}/xsim

${XSIM} --nolog test_cpu.wdb --view test_cpu.wcfg -g
