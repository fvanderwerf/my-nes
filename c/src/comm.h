
#ifndef SRC_COMM_H_
#define SRC_COMM_H_

#include "xil_types.h"

#define COMM_START_CHR '\x02'
#define COMM_END_CHR '\x03'
#define COMM_ESCAPE_CHR '\x1b'

void comm_send(const char *pkt, u32 size);

u32 comm_recv(char *pkt, u32 max_size);

#endif /* SRC_COMM_H_ */
