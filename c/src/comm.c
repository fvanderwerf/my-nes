
#include "comm.h"

#include <stdbool.h>

#include "xil_printf.h"

static u32 comm_recv_frame(char *pkt, u32 max_size, u32 *expected_crc);

u32 crc32_for_byte(u32 r) {
    for(int j = 0; j < 8; ++j)
        r = (r & 1? 0: (u32)0xEDB88320L) ^ r >> 1;
    return r ^ (u32)0xFF000000L;
}

void crc32(const void *data, size_t n_bytes, u32* crc) {
    static u32 table[0x100];
    if(!*table)
        for(size_t i = 0; i < 0x100; ++i)
            table[i] = crc32_for_byte(i);
    for(size_t i = 0; i < n_bytes; ++i)
        *crc = table[(u8)*crc ^ ((u8*)data)[i]] ^ *crc >> 8;
}

void comm_send_data_byte(char c)
{
	if (c == COMM_START_CHR || c == COMM_END_CHR || c == COMM_ESCAPE_CHR)
		outbyte(COMM_ESCAPE_CHR);

	outbyte(c);
}

void comm_send_data(const char *data, u32 size)
{
	for (u32 i = 0; i < size; i++)
		comm_send_data_byte(data[i]);
}

void comm_send(const char *pkt, u32 size)
{
	u32 crc = 0;
	crc32(pkt, size, &crc);

	outbyte(COMM_START_CHR);
	outbyte(COMM_START_CHR);

	comm_send_data_byte((crc >> 24) & 0xff);
	comm_send_data_byte((crc >> 16) & 0xff);
	comm_send_data_byte((crc >> 8) & 0xff);
	comm_send_data_byte((crc >> 0) & 0xff);

	comm_send_data(pkt, size);

	outbyte(COMM_END_CHR);
	outbyte(COMM_END_CHR);
}

u32 comm_recv(char *pkt, u32 max_size)
{
	u32 expected_crc = 0;
	u32 actual_crc = 0;
	u32 size = 0;

	do {
		size = comm_recv_frame(pkt, max_size, &expected_crc);
		crc32(pkt, size, &actual_crc);
	} while (expected_crc != actual_crc);

	return size;
}

static u32 comm_recv_frame(char *pkt, u32 max_size, u32 *expected_crc)
{
	u32 size = 0;
	u32 crc = 0;
	u32 crc_size = 0;

	enum { START_1, START_2, DATA, DATA_ESCAPE, END_1 } state = START_1;

	while (1) {
		char c = inbyte();

        switch (state) {
            case START_1:	/* waiting for first start chr */
                if (c == COMM_START_CHR)
                    state = START_2;
                break;
            case START_2:	/* waiting for second start chr */
                if (c == COMM_START_CHR) {
                    size = 0;
                    crc_size = 0;
                    state = DATA;
                } else {
                    state = START_1;
                }
                break;
            case DATA:
                if (c == COMM_ESCAPE_CHR)
                    state = DATA_ESCAPE;
                else if (c == COMM_END_CHR)
                    if (crc_size != 4)
                        state = START_1; /* crc is not complete, stat over */
                    else
                        state = END_1; /* first end chr */
                else if (c == COMM_START_CHR)
                    /* unexpected, this could be start of new frame */
                    state = START_2;
                else if (size < max_size) {
                    if (crc_size < 4) {
                        crc = (crc << 8) | ((u8) c);
                        crc_size++;
                    } else {
                        pkt[size++] = c;
                    }
                } else
                    /* msg too large for buffer */
                    state = START_1;
                break;
            case DATA_ESCAPE:
                if (size < max_size) {
                    if (crc_size < 4) {
                        crc = (crc << 8) | ((u8) c);
                        crc_size++;
                    } else {
                        pkt[size++] = c;
                    }
                    state = DATA;
                } else {
                    /* msg too large for buffer */
                    state = START_1;
                }
                break;
            case END_1:
                if (c == COMM_END_CHR) {
                    *expected_crc = crc;
                    return size; /* second end chr, msg is complete */
                } else if (c == COMM_START_CHR)
                    /* unexpected, this could be start of new frame */
                    state = START_2;
                else
                    /* start over */
                    state = START_1;
                break;
        }
	}
}

