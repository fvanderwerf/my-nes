
#include <stdio.h>
#include "platform.h"
#include "xil_cache.h"
#include "xil_io.h"
#include "xil_printf.h"
#include "mb_interface.h"
#include "comm.h"

#define CMD_ECHO        1
#define CMD_READ        2
#define CMD_WRITE       3
#define CMD_FLUSH       4
#define CMD_READ_REG    5
#define CMD_WRITE_REG   6

#define CMD_NES_STEP    0x81
#define CMD_NES_ISTEP   0x82

#define NES_REG_BASE        0x10001000
#define NES_CTRL_REG        (NES_REG_BASE + 0x0000)
#define NES_STAT_REG        (NES_REG_BASE + 0x0004)
#define NES_STEP_REG        (NES_REG_BASE + 0x0080)
#define NES_PC_REG          (NES_REG_BASE + 0x0084)
#define NES_IR_REG          (NES_REG_BASE + 0x0088)
#define NES_P_REG           (NES_REG_BASE + 0x008c)
#define NES_A_REG           (NES_REG_BASE + 0x0090)
#define NES_S_REG           (NES_REG_BASE + 0x0094)
#define NES_X_REG           (NES_REG_BASE + 0x0098)
#define NES_Y_REG           (NES_REG_BASE + 0x009c)


char recv_buffer[4096];


static void nes_step(void)
{
    Xil_Out32(NES_CTRL_REG, 0);

    /* wait until not busy anymore */
    while (Xil_In32(NES_STAT_REG) & 0x01)
        ;

    /* a rising edge on the step bit triggers a single step */
    Xil_Out32(NES_CTRL_REG, 2);

}

static u32 nes_istep(void)
{
    u32 ncycles = 0;

    /* Start with a step if we're at the beginning of an instruction */
    if (Xil_In32(NES_STEP_REG) == 0x80) {
        nes_step();
        ncycles++;
    }

    /* Continue until we're at the beginning of the next instruction */
    while (Xil_In32(NES_STEP_REG) != 0x80) {
        nes_step();
        ncycles++;
    }

    return ncycles;
}

static void nes_status(char *buf, u32 ncycles)
{
    u32 x32 = 0;
    u16 x16 = 0;
    u8 x8 = 0;

    /* pc */
    x16 = Xil_In32(NES_PC_REG);
    x16 = Xil_Htons(x16);
    memcpy(buf, &x16, sizeof(x16));
    buf += sizeof(x16);

    /* a */
    x8 = Xil_In32(NES_A_REG);
    memcpy(buf, &x8, sizeof(x8));
    buf += sizeof(x8);

    /* x */
    x8 = Xil_In32(NES_X_REG);
    memcpy(buf, &x8, sizeof(x8));
    buf += sizeof(x8);

    /* y */
    x8 = Xil_In32(NES_Y_REG);
    memcpy(buf, &x8, sizeof(x8));
    buf += sizeof(x8);

    /* p */
    x8 = Xil_In32(NES_P_REG);
    memcpy(buf, &x8, sizeof(x8));
    buf += sizeof(x8);

    /* s */
    x8 = Xil_In32(NES_S_REG);
    memcpy(buf, &x8, sizeof(x8));
    buf += sizeof(x8);

    /* cyc */
    x32 = Xil_Htonl(ncycles);
    memcpy(buf, &x32, sizeof(x32));
}

int main()
{
    init_platform();

    while (1) {
        u32 recv_size = comm_recv(recv_buffer, sizeof(recv_buffer));
        u32 size = 0;
        u32 addr = 0;
        volatile u32 *reg = NULL;
        u32 value = 0;

        if (recv_size < 1)
            continue;

        switch ((u8) recv_buffer[0]) {
            case CMD_ECHO:
                comm_send(recv_buffer, recv_size);
                break;
            case CMD_READ:
                if (recv_size != 9)
                    continue;

                for (int i = 0; i < 4; i++)
                    addr = (addr << 8) | (u8) (recv_buffer[1+i]);

                for (int i = 0; i < 4; i++)
                    size = (size << 8) | (u8) (recv_buffer[5+i]);

                comm_send((char *) addr, size);
                break;
            case CMD_WRITE:
                if (recv_size < 5)
                    continue;

                for (int i = 0; i < 4; i++)
                    addr = (addr << 8) | (u8) (recv_buffer[1+i]);

                memcpy((char *) addr, recv_buffer + 5, recv_size - 5);
                comm_send(NULL, 0);

                break;
            case CMD_FLUSH:
                if (recv_size != 9)
                    continue;

                for (int i = 0; i < 4; i++)
                    addr = (addr << 8) | (u8) (recv_buffer[1+i]);

                for (int i = 0; i < 4; i++)
                    size = (size << 8) | (u8) (recv_buffer[5+i]);

                microblaze_flush_dcache_range(addr, size);
                mbar(0);
                comm_send(NULL, 0);

                break;
            case CMD_READ_REG:
                if (recv_size != 5)
                    continue;

                for (int i = 0; i < 4; i++)
                    addr = (addr << 8) | (u8) (recv_buffer[1+i]);

                reg = (volatile u32 *) addr;
                value = Xil_Htonl(*reg);
                comm_send(&value, sizeof(value));
                break;

            case CMD_WRITE_REG:
                if (recv_size != 9)
                    continue;

                for (int i = 0; i < 4; i++)
                    addr = (addr << 8) | (u8) (recv_buffer[1+i]);

                for (int i = 0; i < 4; i++)
                    value = (value << 8) | (u8) (recv_buffer[5+i]);

                reg = (volatile u32 *) addr;
                *reg = value;

                comm_send(NULL, 0);
                break;

            case CMD_NES_STEP:
                if (recv_size != 1)
                    continue;

                nes_step();
                nes_status(recv_buffer, 1);
                comm_send(recv_buffer, 11);
                break;

            case CMD_NES_ISTEP:
                if (recv_size != 1)
                    continue;

                nes_status(recv_buffer, nes_istep());
                comm_send(recv_buffer, 11);
                break;
        }
    }

    cleanup_platform();
    return 0;
}
