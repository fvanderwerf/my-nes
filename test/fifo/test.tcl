
read_vhdl -vhdl2008 {
    ./src/gray.vhdl
    ./src/fifo.vhdl
    ./test/fifo/test_fifo.vhdl
}

set_property top fifo_test_top [current_fileset -simset]
export_simulation -simulator xsim -force -directory ./test/fifo/sim

exec -- sh -c "cd ./test/fifo/sim/xsim && ./fifo_test_top.sh" >@stdout
