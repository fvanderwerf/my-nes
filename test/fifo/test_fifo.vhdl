
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fifo_test_top is
end fifo_test_top;

architecture behavioral of fifo_test_top is
    constant width : integer := 4;

    signal areset : std_logic;

    -- write interface
    signal wclk : std_logic;
    signal wrdy : std_logic;
    signal wen : std_logic;
    signal wdata : std_logic_vector(width - 1 downto 0);
    signal full : std_logic;

    -- read interface
    signal rclk : std_logic;
    signal rrdy : std_logic;
    signal ren : std_logic;
    signal rdata : std_logic_vector(width - 1 downto 0);
    signal empty : std_logic;
begin

    fifo : entity work.fifo
        generic map(
            width => width,
            depth_shift => 4) -- depth is 16
        port map(
            areset => areset,

            wclk => wclk,
            wrdy => wrdy,
            wen => wen,
            wdata => wdata,
            full => full,

            rclk => rclk,
            rrdy => rrdy,
            ren => ren,
            rdata => rdata,
            empty => empty
        );

    process
    begin
        report "FIFO test started" severity note;

        areset <= '1';
        wclk <= '0';
        rclk <= '0';
        wen <= '0';
        ren <= '0';
        
        wait for 1 ns;
        areset <= '0';

        assert wrdy = '0'
            report "FIFO write interface should not be ready"
            severity failure;
        assert rrdy = '0'
            report "FIFO read interface should not be ready"
            severity failure;

        -- run the write clock...
        -- the areset goes to a 2-stage synchronizer. The ready signal
        -- is the inverted synchronized reset.
        for i in 1 to 2
        loop
            wclk <= '1';
            wait for 1 ns;
            wclk <= '0';
            wait for 1 ns;
        end loop;

        -- ... and check the write interface becomes ready
        assert wrdy = '1'
            report "FIFO write interface not ready"
            severity failure;

        -- the fifo should be empty after reset
        assert empty = '1'
            report "FIFO not empty after reset"
            severity failure;

        -- the read clock hasn't cycled yet, so it should still be not ready
        assert rrdy = '0'
            report "FIFO read interface should not be ready yet"
            severity failure;
        for i in 1 to 2
        loop
            rclk <= '1';
            wait for 1 ns;
            rclk <= '0';
            wait for 1 ns;
        end loop;

        -- ... and check the write interface becomes ready
        assert rrdy = '1'
            report "FIFO read interface not ready"
            severity failure;

        -- the fifo should be empty after reset
        assert full = '0'
            report "FIFO is full after reset"
            severity failure;

        -- cycle for a bit longer and check the fifo remains empty
        for i in 1 to 5
        loop
            rclk <= '1';
            wait for 1 ns;
            rclk <= '0';
            wait for 1 ns;
        end loop;

        for i in 1 to 5
        loop
            wclk <= '1';
            wait for 1 ns;
            wclk <= '0';
            wait for 1 ns;
        end loop;

        assert rrdy = '1' and wrdy = '1' and empty = '1' and full = '0'
            report "FIFO initialization is not stable"
            severity failure;

        -- now push data into the fifo
        wdata <= "1010"; 
        wen <= '1';

        wclk <= '1';
        wait for 1 ns;
        wclk <= '0';
        wait for 1 ns;

        wen <= '0';

        -- the data has not been synchronized to the read domain, so empty is still active
        assert empty = '1'
            report "FIFO write should not be in the read domain just yet"
            severity failure;

        -- let the synchronizers do their work
        for i in 1 to 2
        loop
            rclk <= '1';
            wait for 1 ns;
            rclk <= '0';
            wait for 1 ns;
        end loop;

        assert empty = '0'
            report "FIFO write has not been synchronized to read domain"
            severity failure;

        assert rdata = "1010"
            report "FIFO read interface should always output current data"
            severity failure;
        
        -- now pop the item from the fifo, it should then be empty again
        ren <= '1';
        rclk <= '1';
        wait for 1 ns;
        rclk <= '0';
        wait for 1 ns;
        ren <= '0';

        assert empty = '1'
            report "FIFO should be empty again"
            severity failure;


        for i in 0 to 15
        loop
            assert full = '0'
                report "FIFO should not be full yet: " & integer'image(i)
                severity failure;

            wen <= '1';
            wdata <= std_logic_vector(to_unsigned(i, 4));

            wclk <= '1';
            wait for 1 ns;
            wclk <= '0';
            wait for 1 ns;

            wen <= '0';
        end loop;

        assert full = '1'
            report "FIFO should be full by now"
            severity failure;

        -- read clock hasn't run, so it still thinks it is empty
        assert empty = '1'
            report "Read domain should still think the FIFO is empty"
            severity failure;

        -- clock the write index to the read domain
        for i in 0 to 1 loop
            rclk <= '1';
            wait for 1 ns;
            rclk <= '0';
            wait for 1 ns;
        end loop;

        assert empty = '0'
            report "Read domain should know it is not empty now"
            severity failure;

        for i in 0 to 15 loop
            assert rdata = std_logic_vector(to_unsigned(i, 4));
            ren <= '1';
            rclk <= '1';
            wait for 1 ns;
            rclk <= '0';
            wait for 1 ns;
            ren <= '0';
        end loop;

        assert empty = '1'
            report "All data has been read, FIFO should be empty"
            severity failure;

        report "FIFO test completed" severity note;
        wait;
    end process;
end behavioral;
