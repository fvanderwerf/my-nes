library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
use ieee.std_logic_textio.all;

use IEEE.NUMERIC_STD.ALL;


entity testmem is
    generic(
        filename : string := "");       
    port(
        clk : in std_logic;        
        addr : in std_logic_vector(15 downto 0);
        dout : out std_logic_vector(7 downto 0);
        din : in std_logic_vector(7 downto 0);
        we : in std_logic);
end testmem;

architecture behavioral of testmem is
    type memtype is array(65535 downto 0) of std_logic_vector(7 downto 0);     
    
     -- Read a *.hex file
    impure function load_mem(fn : string) return memtype is
        file filehandle       : text;
        variable curline      : line;
        variable value        : std_logic_vector(7 downto 0);
        variable result       : memtype := (others => (others => '0'));

    begin
        file_open(filehandle, fn, read_mode);
         
        for i in 0 to memtype'length loop
            exit when endfile(filehandle);
        
            readline(filehandle, curline);
            hread(curline, value);
            result(i) := value;
        end loop;
        return Result;
    end function;
    
    signal mem : memtype := load_mem(filename);
    signal prev_reset : std_logic;     
begin

    dout <= mem(to_integer(unsigned(addr)));

    process(clk)
    begin
        if rising_edge(clk) then
            if we = '1' then
                mem(to_integer(unsigned(addr))) <= din;
            end if;
        end if;
    end process;
end behavioral;
