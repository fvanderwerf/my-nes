
library ieee;
use ieee.std_logic_1164.all;

use ieee.numeric_std.all;

entity test_nes_cpu is
end test_nes_cpu;

architecture structural of test_nes_cpu is
    signal areset : std_logic;
    signal clk : std_logic;

    signal addr : std_logic_vector(15 downto 0);
    signal din : std_logic_vector(7 downto 0);
    signal dout : std_logic_vector(7 downto 0);
    signal we : std_logic;
    signal sync : std_logic;

    signal pc : std_logic_vector(15 downto 0);
    signal ir : std_logic_vector(7 downto 0);
    signal p : std_logic_vector(7 downto 0);
    signal a : std_logic_vector(7 downto 0);
    signal s : std_logic_vector(7 downto 0);
    signal x : std_logic_vector(7 downto 0);
    signal y : std_logic_vector(7 downto 0);

begin

    cpu : entity work.nes_cpu
        port map (
            clk => clk,
            areset => areset,
            addr => addr,
            din => din,
            dout => dout,
            we => we,
            sync => sync,
            pc => pc,
            ir => ir,
            p => p,
            a => a,
            s => s,
            x => x,
            y => y);

    mem : entity work.testmem
        generic map (
            filename => "nestest_cpu.hex")
        port map(
            clk => clk,
            addr => addr,
            din => dout,
            dout => din,
            we => we);

    logger : entity work.cpu_logger
        generic map(
            filename => "test_nestest.log")
        port map(
            clk => clk,
            areset => areset,
            sync => sync,
            pc => pc,
            ir => ir,
            p => p,
            a => a,
            s => s,
            x => x,
            y => y);

    process
    begin
        -- first reset
        areset <= '1';
        clk <= '0';

        for i in 0 to 3 loop
            wait for 5ns;
            clk <= not clk;
        end loop;

        -- release reset
        areset <= '0';

        -- run clk ad infinitum
        loop
            wait for 5ns;
            clk <= not clk;
        end loop;

    end process;

end structural;
