
import re
import sys

def check_logs(reference, actual):

    with open(reference, "r") as reflog, open(actual, "r") as reallog:
        reflines = reflog.readlines()
        reallines = reallog.readlines()

    # skip the first realline because the reference does not
    # log the initial boot sequence
    for ref, real in zip(reflines, reallines[1:]):
        
        # parse reference
        match = re.fullmatch(
            "(?P<pc>^[0-9A-Fa-f]{4,4}).*"
            "A:(?P<a>[0-9A-Fa-f]{2,2}).*"
            "X:(?P<x>[0-9A-Fa-f]{2,2}).*"
            "Y:(?P<y>[0-9A-Fa-f]{2,2}).*"
            "P:(?P<p>[0-9A-Fa-f]{2,2}).*"
            "SP:(?P<sp>[0-9A-Fa-f]{2,2}).*"
            "CYC:(?P<cyc>[0-9]+).*",
            ref.strip())

        # build ref dict
        refdict = {
            "pc": int(match.group("pc"), base=16),
            "a": int(match.group("a"), base=16),
            "x": int(match.group("x"), base=16),
            "y": int(match.group("y"), base=16),
            "p": int(match.group("p"), base=16),
            "sp": int(match.group("sp"), base=16),
            "cyc": int(match.group("cyc"))
        }

        match = re.fullmatch(
            "(?P<pc>^[0-9A-Fa-f]{4,4}).*"
            "A:(?P<a>[0-9A-Fa-f]{2,2}).*"
            "X:(?P<x>[0-9A-Fa-f]{2,2}).*"
            "Y:(?P<y>[0-9A-Fa-f]{2,2}).*"
            "P:(?P<p>[0-9A-Fa-f]{2,2}).*"
            "SP:(?P<sp>[0-9A-Fa-f]{2,2}).*"
            "CYC:(?P<cyc>[0-9]+).*",
            real.strip())

        realdict = {
            "pc": int(match.group("pc"), base=16),
            "a": int(match.group("a"), base=16),
            "x": int(match.group("x"), base=16),
            "y": int(match.group("y"), base=16),
            "p": int(match.group("p"), base=16),
            "sp": int(match.group("sp"), base=16),
            "cyc": int(match.group("cyc"))
        }

        assert refdict.keys() == realdict.keys()

        error = False
        for key in realdict.keys():
            if realdict[key] != refdict[key]:
                print("CPU state mismatch {}, "
                      "expected={:04x}, actual={:04x}:".format(
                            key, refdict[key], realdict[key]))
                error = True

        print(ref, end="")

        if error:
            return
 

if __name__ == "__main__":
    check_logs(sys.argv[1], sys.argv[2])
