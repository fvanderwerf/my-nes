
read_vhdl -vhdl2008 {
    ./src/nes_alu.vhdl
    ./src/nes_cpu.vhdl
    ./test/nestest/cpu_logger.vhdl
    ./test/nestest/testmem.vhdl
    ./test/nestest/test_nestest.vhdl
}

set_property top test_nes_cpu [current_fileset -simset]
export_simulation -simulator xsim -force -directory ./test/nestest/sim

exec -- sh -c "cd ./test/nestest/sim/xsim && ./test_nes_cpu.sh" >@stdout
