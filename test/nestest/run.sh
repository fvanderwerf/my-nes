#!/bin/sh

set -ex

# compile
vhdl_files="
../../src/nes_alu.vhdl
../../src/nes_cpu.vhdl
cpu_logger.vhdl
testmem.vhdl
test_nestest.vhdl"

xvhdl --nolog --2008 $vhdl_files

xelab --debug typical test_nes_cpu --nolog

xsim test_nes_cpu -key {Behavioral:sim_1:Functional:test_nes_cpu} -tclbatch cmd.tcl --nolog --wdb test_nes_cpu.wdb

python3 verify.py nestest.log test_nestest.log
