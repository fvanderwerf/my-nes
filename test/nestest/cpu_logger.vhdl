library ieee;
use IEEE.STD_LOGIC_1164.ALL;
use std.textio.all;
use ieee.std_logic_textio.all;

use IEEE.NUMERIC_STD.ALL;

entity cpu_logger is
    generic(
        filename : string);
    port (
        areset : in std_logic;        
        clk : in std_logic;
        sync : in std_logic;
        pc : in std_logic_vector(15 downto 0);
        ir : in std_logic_vector(7 downto 0);
        p : in std_logic_vector(7 downto 0);
        a : in std_logic_vector(7 downto 0);
        s : in std_logic_vector(7 downto 0);
        x : in std_logic_vector(7 downto 0);
        y : in std_logic_vector(7 downto 0)
    );
end cpu_logger;

architecture Behavioral of cpu_logger is

    function to_hex4(x : std_logic_vector(3 downto 0)) return string is
    begin
        case x is
            when "0000" => return "0";
            when "0001" => return "1";
            when "0010" => return "2";
            when "0011" => return "3";
            when "0100" => return "4";
            when "0101" => return "5";
            when "0110" => return "6";
            when "0111" => return "7";
            when "1000" => return "8";
            when "1001" => return "9";
            when "1010" => return "A";
            when "1011" => return "B";
            when "1100" => return "C";
            when "1101" => return "D";
            when "1110" => return "E";
            when "1111" => return "F";
        end case;
    end function;

    function to_hex8(x : std_logic_vector(7 downto 0)) return string is
    begin
        return to_hex4(x(7 downto 4)) & to_hex4(x(3 downto 0));
    end function;

    function to_hex16(x : std_logic_vector(15 downto 0)) return string is
    begin
        return to_hex8(x(15 downto 8)) & to_hex8(x(7 downto 0));
    end function;

begin
    process
        file filehandle : text;
        variable logline : line;
        variable cyc : natural := 0;
    begin
        file_open(filehandle, filename, write_mode);

        loop
            wait until rising_edge(clk);

            if areset ='0' then
                if sync = '1' then
                    write(logline,
                        to_hex16(
                            std_logic_vector(unsigned(pc) - 1)) & " " &
                            to_hex8(ir) & " " &
                            "A:" & to_hex8(a) & " " &
                            "X:" & to_hex8(x) & " " &
                            "Y:" & to_hex8(y) & " " &
                            "P:" & to_hex8(p) & " " &
                            "SP:" & to_hex8(s) & " " &
                            "CYC:" & natural'image(cyc - 1)
                        );
                    writeline(filehandle, logline);
                end if;
                cyc := cyc + 1;
            end if;
        end loop;

        file_close(filehandle);
    end process;
end Behavioral;
