
read_vhdl -vhdl2008 ./src/gray.vhdl
read_vhdl -vhdl2008 ./test/gray/test_gray.vhdl

set_property top gray_test_top [current_fileset -simset]
export_simulation -simulator xsim -force -directory ./test/gray/sim

exec -- sh -c "cd ./test/gray/sim/xsim && ./gray_test_top.sh" >@stdout
