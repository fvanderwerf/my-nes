
library ieee;
use ieee.std_logic_1164.all;

entity gray_test_top is
end entity;

architecture behavioral of gray_test_top is

    constant width : integer := 4;

    signal encode_i : std_logic_vector(width - 1 downto 0);
    signal encode_o : std_logic_vector(width - 1 downto 0);

    signal decode_o : std_logic_vector(width - 1 downto 0);

    type test_vector is array(0 to 15, 0 to 1) of std_logic_vector(width - 1 downto 0);

    constant tests : test_vector := (
            ("0000", "0000"),
            ("0001", "0001"),
            ("0010", "0011"),
            ("0011", "0010"),
            ("0100", "0110"),
            ("0101", "0111"),
            ("0110", "0101"),
            ("0111", "0100"),
            ("1000", "1100"),
            ("1001", "1101"),
            ("1010", "1111"),
            ("1011", "1110"),
            ("1100", "1010"),
            ("1101", "1011"),
            ("1110", "1001"),
            ("1111", "1000")
    );

begin
    gray_encoder : entity work.gray_encoder
        generic map(
            width => width)
        port map(
            i => encode_i,
            o => encode_o);

    gray_decoder : entity work.gray_decoder
        generic map(
            width => width)
        port map(
            i => encode_o,
            o => decode_o);

    process
    begin
        for i in test_vector'left to test_vector'right
        loop
            encode_i <= tests(i, 0);
            wait for 1 ns;
            assert encode_o = tests(i, 1)
                report "Failed gray encode test " & integer'image(i)
                severity failure;
            
            assert decode_o = tests(i, 0)
                report "Failed gray decode test " & integer'image(i)
                severity failure;
        end loop;

        wait;
    end process;
end behavioral;

