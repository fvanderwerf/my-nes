create_project -in_memory -part xc7a100tcsg324-1

read_checkpoint synth.dcp
link_design

read_xdc -unmanaged ./src/physical.xdc

place_design
phys_opt_design

write_mem_info -force system.mmi
write_hwdef -force system.hdf

route_design
phys_opt_design

check_timing
report_timing -file placeroute-timing.txt -warn_on_violation -max_paths 100

write_checkpoint -force placeroute
