
VIVADO=vivado -nojournal -nolog
XSCT=xsct
UPDATEMEM=updatemem

all: nes_sw.bit

.PHONY: synth
synth: synth.dcp

.PHONY: placeroute
placeroute: placeroute.dcp

synth.dcp: synth.tcl
	$(VIVADO) -mode batch -source $(abspath $<)

system.hdf system.mmi placeroute.dcp: placeroute.tcl ./src/physical.xdc synth.dcp
	$(VIVADO) -mode batch -source $(abspath $<)

nes.bit: bitstream.tcl placeroute.dcp
	$(VIVADO) -mode batch -source $(abspath $<)

software workspace/loader/Debug/loader.elf: sw.tcl system.hdf system.mmi c/src/*
	$(XSCT) sw.tcl

nes_sw.bit: nes.bit workspace/loader/Debug/loader.elf
	$(UPDATEMEM) -force -meminfo system.mmi -data ./workspace/loader/Debug/loader.elf -proc system_i/softcore --bit nes.bit --out $@

.PHONY: run
run: run.tcl nes_sw.bit
	$(VIVADO) -mode batch -source $(abspath $<)

.PHONY: test_gray
test_gray: ./test/gray/test.tcl ./test/gray/test_gray.vhdl
	$(VIVADO) -mode batch -source $(abspath $<)

.PHONY: test_fifo
test_fifo: ./test/fifo/test.tcl ./test/fifo/test_fifo.vhdl
	$(VIVADO) -mode batch -source $(abspath $<)

.PHONY: test_nestest
test_nestest:
	cd test/nestest && ./run.sh

.PHONY: clean
clean:
	rm -rf .Xil .cxl.ip .srcs .ip_user_files
	rm -rf synth.dcp placeroute.dcp
	rm -rf nes.bit
	rm -rf synth.mk
	rm -rf synth-timing.txt placeroute-timing.txt
	rm -rf ./test/gray/sim/ ./test/fifo/sim/
	rm -rf bd system.hdf system.mmi
	rm -rf workspace

-include synth.mk
