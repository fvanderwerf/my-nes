exec rm -rf workspace
setws workspace

createhw -name hw -hwspec system.hdf
createbsp -name bsp -proc softcore -hwproject hw
createapp -name loader -app {Empty Application} -proc softcore -hwproject hw -bsp bsp -lang c

importsources -name loader -path c/src -linker-script

projects -build
